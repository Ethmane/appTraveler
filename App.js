
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Navigator, NativeModules, StatusBar } from 'react-native';
import { TabNavigator, StackNavigator,DrawerNavigator} from 'react-navigation';
import { Font } from 'expo';

import Settings from './components/Settings';

import Traveler from './components/screens/Profile/Traveler';
import Profile from './components/screens/Profile/Profile';
import ProfileContact from './components/screens/Profile/ProfileContact';
import PotenielContact from './components/screens/Profile/PotenielContact';

import Friends from './components/Friends';
import Messages from './components/Messages';
import Notifications from './components/Notifications';
import TimeLine from './components/TimeLine';


import Login from './components/screens/Authentification/Login';
import SignUp from './components/screens/Authentification/SignUp';
import ForgetPassword from './components/screens/Authentification/ForgetPassword';

import WaitingConfirmation from './components/screens/Authentification/WaitingConfimation.js';
import ContentMessage  from './components/ContentMessage';
import Post from './components/Post';
import Example from './components/Example';
import {I18n} from './i18n/i18n';



const ContentMessageStack = StackNavigator({
    ViewContentMessage : {
        screen : ContentMessage,
    },
});

const PotenielContactStack = StackNavigator({
    ViewPotenielFriend : {
        screen : PotenielContact,
    },
});
  
const travelerStack = StackNavigator({
  ViewTraveler : {
      screen : Traveler,
  },

}); 

const settingsStack = StackNavigator({
  viewSettings: {
      screen : Settings,
  },
}); 

const profileStack = StackNavigator({
  viewProfile :{
    screen : Profile
  }
});

const friendsStack = StackNavigator({
  viewFriends : {
    screen : Friends
  }
});

const postStack = StackNavigator({
  viewPost : {
    screen : Post
  }
});

const messagesStack = StackNavigator({
  viewMessages  : {
    screen : Messages
  },
  ViewContentMessage : {
    screen : ContentMessage,
},
});

const notificationsStack = StackNavigator({
  viewNotifications : {
    screen : Notifications
  }
});

const TimeLineStack = StackNavigator({   
  viewTimeLine : {
    screen : TimeLine
  }
});

const ProfileContactStack = StackNavigator({
  viewProfileContact : {
    screen : ProfileContact
  }
});
      
const LoginStack = StackNavigator({
  viewLogin: {
    screen : Login
  }
});

const SignUpStack = StackNavigator({
  viewSignUp: {
    screen : SignUp
  }
});

const ForgetPasswordStack = StackNavigator({
  viewForgetPassword: {
    screen : ForgetPassword
  }
});

const WaitingConfirmationStack = StackNavigator({
  viewWaitingConfirmation: {
    screen : WaitingConfirmation
  }
});

const ExampleStack = StackNavigator({
  viewExample: {
    screen : Example
  }
});

//Example



const MyDrawer = DrawerNavigator({

  ViewLogin: {
    screen: LoginStack,
    title : `${I18n.t('nameScreenLogin')}`,
  },



  ViewSignUp: {
    screen: SignUpStack,
    title : `${I18n.t('nameScreenSignUp')}`,
  },
  
  ViewForgetPassword: {
    screen: ForgetPasswordStack,
    title : `${I18n.t('nameScreenForgetPassword')}`,
  },

  ViewTraveler: {
    screen: travelerStack,
    title : 'TRAVELER'
  },
  
  ViewPotenielFriend: {
    screen: PotenielContactStack,
    title : 'PotenielFriend'
  },

  ViewTimeline: {
    screen: TimeLineStack,
    title : 'TIMELINE'
  },
  
  ViewExample: {
    screen: ExampleStack,
    title : 'CHAT'
  },

  ViewProfile: {
    screen: profileStack,
    title : 'PROFILE'
  },
  
  ViewNotifications: {
    screen: notificationsStack,
    title : 'NOTIFICATIONS'
  },
  ViewMessages: {
    screen: messagesStack,
    title : 'MESSAGES'
  },
  ViewFriends: {
    screen: friendsStack,
    title : 'FRIENDS'
  },
  ViewPost: {
    screen: postStack,
    title : 'POST'
  },
  ViewSettings: {
    screen: settingsStack,
    title : 'SETTINGS'
  },
  ViewProfileContact: {
    screen: ProfileContactStack,
    title : 'PROFILE CONTACT'
  },
 
  
});




export default class App extends React.Component {

   componentDidMount() {
      Font.loadAsync({
        Roboto: require('native-base/Fonts/Roboto.ttf'),
        Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      })
   }
  render() {
    return (
      <View style = {styles.container} >
            <StatusBar hidden = {true}/>
            <MyDrawer/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
});
