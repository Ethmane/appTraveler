export default {

    /** Ecran CHAT et ses labels  */
    nameScreenChat : 'CHAT',



    /** Ecran NOTIFICATIONS et ses labels */
    nameScreenNotifications : 'NOTIFICATIONS',



    /** Ecran MESSAGES et ses labels  */
    nameScreenMessages : 'MESSAGES',



    /** Ecran POSTS et ses labels  */
    nameScreenPosts : 'POSTS',





    /** Ecran AMIS et ses labels */
    nameScreenFriends : 'AMIS',





    /** Ecran Profile  et ses labels */
    nameScreenProfile: 'PROFIL',
    firstNameProfile : 'Prenom',
    nameProfile : 'Nom',
    descProfile : 'Description',
    companyProfile : 'Entreprise',


   /** Ecran POTENIEL CONTACT et ses labels */
   nameScreenPotentielFriend : 'POTENIEL CONTACT',

    /** Ecran INSCRIPTION et ses labels */
    nameScreenSignUp : "INSCRIPTION",
    _msg_error_type1 : 'Il existe dejà un compte avec cette adresse mail',
    _msg_error_type2 : "L'adresse mail n'est pas valide !",
    _msg_error_type3 : 'Les comptes ne sont pas encore activés! Pensez-y',
    _msg_error_type4 : 'Le mot de passe est assez faible !',
    _msg_error_password_format : `Mot de Passe : Format invalide 
    doit contenir :
    - au moins 8 caratères
    - au moins une Lettre Majuscule
    - au moins une Chiffre`,
    _msg_error_password_confirm : 'Mot de Passe Confirmation : Format invalide',
    _btn_signup : "S'enregistrer",
    _msg_error_password_no_identic : 'Mots de passes non identiques',
    _lb_have_ready_account : 'Si vous avez déjà un compte:',
    _txt_password_confirm : 'Confirmation de mot de passe',

    nameScreenLogin : "CONNEXION",
    lb_mail : 'Email',
    lb_password : 'Mot de passe',
    btn_login :'Connexion',
    lb_create_account : 'Creer un nouveau compte:',
    lb_click_here : 'Cliquez ici',
    msg_error_empty : 'Mot de passe vide',
    msg_error_mail: 'Email : Format invalide',
    msg_error_auth: "Echec d'autentification",
    lb_forget_id: "Mot de passe oublié",
    

    /**Ecran Mot de passe oublié */
    nameScreenForgetPassword :'Recuperation Mot de Passe',
    btn_send : 'Envoyer',
    msg_error_unknown_email : 'Email inconnu',


    /** Ecran FRIENDS et ses labels  */
    nameScreenPotentielFriend : 'FRIENDS',




    /** Ecran FIL D'ACTUALITÉS et ses labels */
    nameScreenTimeline : "FIL D'ACTUALITÉS",




    /** Ecran PARAMETRES et ses labels */
    nameScreenSettings : "PARAMETRES",



   
    
    
  };