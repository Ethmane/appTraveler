import I18n from 'ex-react-native-i18n';
//import { getLanguages } from 'react-native-i18n';

import en from './en';
import fr from './fr';  
import ar from './ar';  
import pl from './pl';  

// Enable fallbacks if you want `en-US` and `en-GB` to fallback to `en`
I18n.fallbacks = true;
I18n.locale = "fr";

I18n.translations = {
  en,
  fr, 
  ar, 
  pl
};

/*getLanguages().then(languages => {
  console.log(languages) // ['en-US', 'en']
})*/

//`${I18n.t('nameScreenLogin')}`

export {I18n} ;

