export default {
  
      /** Ecran CHAT et ses labels  */
      nameScreenChat : 'CHAT',
  
  
  
      /** Ecran NOTIFICATIONS et ses labels */
      nameScreenNotifications : 'NOTIFICATIONS',
  
  
  
      /** Ecran MESSAGES et ses labels  */
      nameScreenMessages : 'MESSAGES',
  
  
  
      /** Ecran POSTS et ses labels  */
      nameScreenPosts : 'POSTS',
  
  
  
  
  
      /** Ecran AMIS et ses labels */
      nameScreenFriends : 'AMIS',
  
  
  
  
  
      /** Ecran POTENIEL CONTACT et ses labels */
      nameScreenPotentielFriend : 'POTENIEL CONTACT',
  
  
  
  
      /** Ecran INSCRIPTION et ses labels */
      nameScreenSignUp : "التسجيل",
      _msg_error_type1 : 'هناك بالفعل حساب مع هذا عنوان البريد الإلكتروني',
      _msg_error_type2 : "! عنوان البريد الإلكتروني غير صالح",
      _msg_error_type3 : 'لم يتم تنشيط الحسابات حتى الآن! التفكير في الامر',
      _msg_error_type4 : 'كلمة المرور ضعيفة جدا!',
      _msg_error_password_format : `:كلمة المرور: تنسيق غير صالح
                                             : يجب أن تحتوي على
                            - 8 أحرف على الأقل
                            - حرف واحد على الأقل
                            - رقم واحد على الأقل`,
      _msg_error_password_confirm : 'تأكيد كلمة المرور: تنسيق غير صالح',
      _btn_signup : "التسجيل",
      _msg_error_password_no_identic : 'كلمات مرور غير متطابقة',
      _lb_have_ready_account : 'إذا كان لديك حساب بالفعل',
      _txt_password_confirm : 'تأكيد كلمة المرور',
  
  
  
  
      /** Ecran CHAT et ses labels */
      nameScreenLogin : "تسجيل الدخول",
      lb_mail : 'البريد الإلكتروني',
      lb_password : 'كلمة السر',
      btn_login :'الدخول',
      lb_create_account : 'انشاء حساب جديد',
      lb_click_here : 'انقر هنا',
      msg_error_empty : 'كلمة المرور فارغة',
      msg_error_mail: 'البريد الإلكتروني : تنسيق غير صالح',
      msg_error_auth: " فشل المصادقة",
      lb_forget_id: "كلمة المرور المنسية",
  
  
  
  
  
  
      /** Ecran FRIENDS et ses labels  */
      nameScreenPotentielFriend : 'FRIENDS',
  
  
  
  
      /** Ecran FIL D'ACTUALITÉS et ses labels */
      nameScreenTimeline : "FIL D'ACTUALITÉS",
  
  
  
  
      /** Ecran PARAMETRES et ses labels */
      nameScreenSettings : "PARAMETRES",
  
  
  
     
      
      
    };