export default {
  
      /** Ecran CHAT et ses labels  */
      nameScreenChat : 'CHAT',
  
  
  
      /** Ecran NOTIFICATIONS et ses labels */
      nameScreenNotifications : 'NOTIFICATIONS',
  
  
  
      /** Ecran MESSAGES et ses labels  */
      nameScreenMessages : 'MESSAGES',
  
  
  
      /** Ecran POSTS et ses labels  */
      nameScreenPosts : 'POSTS',
  
  
  
  
  
      /** Ecran AMIS et ses labels */
      nameScreenFriends : 'AMIS',
  
  
  
  
  
      /** Ecran POTENIEL CONTACT et ses labels */
      nameScreenPotentielFriend : 'POTENIEL CONTACT',
  
  
  
  
      /** Ecran INSCRIPTION et ses labels */
      nameScreenSignUp : "INSCRIPTION",
  
  
  
  
  
      /** Ecran CHAT et ses labels */
      nameScreenLogin : "ZALOGUJ SIĘ",
      lb_mail : 'E-mail',
      lb_password : 'Hasło',
      btn_login :'ZALOGUJ SIĘ',
      lb_create_account : 'Stwórz nowe konto',
      lb_click_here : 'Kliknij tutaj',
      msg_error_empty : 'Hasło puste',
      msg_error_mail: 'E-mail: format nieprawidłowy',
      msg_error_auth: 'Błąd uwierzytelniania',
      lb_forget_id: "Zapomniałem hasła",
  
  
  
  
      /** Ecran FRIENDS et ses labels  */
      nameScreenPotentielFriend : 'FRIENDS',
  
  
  
  
      /** Ecran FIL D'ACTUALITÉS et ses labels */
      nameScreenTimeline : "FIL D'ACTUALITÉS",
  
  
  
  
      /** Ecran PARAMETRES et ses labels */
      nameScreenSettings : "PARAMETRES",
  
  
  
     
      
      
    };