export default {
  
      /** Ecran CHAT et ses labels  */
      nameScreenChat : 'CHAT',
  
  
  
      /** Ecran NOTIFICATIONS et ses labels */
      nameScreenNotifications : 'NOTIFICATIONS',
  
  
  
      /** Ecran MESSAGES et ses labels  */
      nameScreenMessages : 'MESSAGES',
  
  
  
      /** Ecran POSTS et ses labels  */
      nameScreenPosts : 'POSTS',
  
  
  
  
  
      /** Ecran AMIS et ses labels */
      nameScreenFriends : 'AMIS',
  
  
  
      /** Ecran Profile  et ses labels */
      nameScreenProfile: 'PROFILE',
      firstNameProfile : 'First Name',
      nameProfile : 'Name',
      descProfile : 'Description',
      companyProfile : 'Company',
  
      /** Ecran POTENIEL CONTACT et ses labels */
      nameScreenPotentielFriend : 'POTENIEL CONTACT',
  
  
  
  
      /** Ecran INSCRIPTION et ses labels */
      nameScreenSignUp : "SIGN UP",
      _msg_error_type1 : 'There is already an account with this email address',
      _msg_error_type2 : "The email address is invalid !",
      _msg_error_type3 : 'Accounts are not activated yet! Think about it',
      _msg_error_type4 : 'The password is pretty weak!',
      _msg_error_password_format : `Password: Invalid format
        must contain :
        - at least 8 characters
        - at least one capital letter
        - at least one digit`,
      _msg_error_password_confirm : 'Password Confirmation: Invalid format',
      _btn_signup : "Sign Up",
      _msg_error_password_no_identic : 'Non-identical passwords',
      _lb_have_ready_account : 'If you already have an account:',
      _txt_password_confirm : 'Password confirmation',
  
      nameScreenLogin : "LOGIN",
      lb_mail : 'Email',
      lb_password : 'Password',
      btn_login :'LOGIN',
      lb_create_account : 'Create a new account',
      lb_click_here : 'Click here',
      msg_error_empty : 'Empty password',
      msg_error_mail: 'Email : invalid Format',
      msg_error_auth: "Authentication Failure",
      lb_forget_id: "Forgot your password",
  

      /**Ecran Mot de passe oublié */
      nameScreenForgetPassword :'Password Recovery',
      btn_send : 'Send',
      msg_error_unknown_email : 'unknown mail',

      /** Ecran FRIENDS et ses labels  */
      nameScreenPotentielFriend : 'FRIENDS',
  

  
      /** Ecran FIL D'ACTUALITÉS et ses labels */
      nameScreenTimeline : "FIL D'ACTUALITÉS",
  
  
  
  
      /** Ecran PARAMETRES et ses labels */
      nameScreenSettings : "PARAMETRES",
  
  
  
     
      
      
    };