const ReverseContentForLanguages = ['ar']

export  default function needReverseContent(languageCurrent) {
    return ReverseContentForLanguages.includes(languageCurrent);
}