import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyC8GoFBp5i1zpq1P-s3BB-AJrzvQisMpQc",
    authDomain: "travaeltogether.firebaseapp.com",
    databaseURL: "https://travaeltogether.firebaseio.com",
    projectId: "travaeltogether",
    storageBucket: "",
    messagingSenderId: "564000833313"
};

firebase.initializeApp(config);

export default firebase;