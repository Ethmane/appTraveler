import firebase from './connexion';


/* Connexion à la l'application */

const Authentification  =(email, password) => firebase.auth().signInWithEmailAndPassword(email,password);
const Inscription = (email, password) => firebase.auth().createUserWithEmailAndPassword(email, password);
const GetPasswordByEmail = (email) => firebase.auth().sendPasswordResetEmail(email)
                    

export {Authentification, Inscription, GetPasswordByEmail}