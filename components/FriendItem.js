//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet,TouchableWithoutFeedback } from 'react-native';
import { Avatar } from 'react-native-elements';
// create a component
import { StackNavigator} from 'react-navigation';
import Traveler from './screens/Profile/Traveler';
import {APP_COLORS} from '../style/color';


const travelerStack = StackNavigator({
    viewTraveler : {
        screen : Traveler,
    },
  
  }); 

class FriendItem extends Component {
    constructor (props) {
        super(props),
        this.state={

        }
        
    }
    handleOnPress = () => {
    //console.log("Youpii");
    this.props.navigation.navigate('viewTraveler');
    }
    render() {
        const { item, itemDesc } = styles;
        return (
            <TouchableWithoutFeedback  onPress={this.handleOnPress}>
                <View style={item}>
                <Avatar
                small
                rounded
                source={require('./icons/plus.png')}
                activeOpacity={0.7} 
                />
                <View style={itemDesc}>
                    <Text >{this.props.data.name}</Text>
                    <Text>{this.props.data.surname}</Text>
                </View>
                </View>
            </TouchableWithoutFeedback>
            
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        padding: 30,
        backgroundColor: '#f1f1f1',
        borderColor: 'gray',
        borderWidth: 1,
        borderTopWidth: 0,
    },
    itemDesc: {
        flexDirection: 'column',
        paddingLeft: 15,
               
    }
});

//make this component available to the app
export default FriendItem;

