import { DrawerItems } from 'react-navigation';
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

const CustomDrawerContentComponent = (props) => (
  <View style={styles.container}>
    <DrawerItems {...props} />
  </View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default CustomDrawerContentComponent;
