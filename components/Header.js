//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title } from 'native-base';
import {APP_COLORS} from '../style/color';
// create a component
class HeaderApp extends Component {
    constructor(props){
        super(props);
        this.state = {

        }
    }
    render() {
       
            if (this.props.nameScreen == "Settings"){
                    return  ( 
                    <View style={styles.container}>
                     
                        <Header>
                            <Left>
                                <Button transparent>
                                <Icon name='arrow-back' />
                                </Button>
                            </Left>
                            <Body >
                                <Text style={{justifyContent: 'flex-end'}} >{this.props.nameScreen.toUpperCase()}</Text>
                            </Body>
                            <Right>
                                <Button transparent>
                                   <Text>Done</Text>
                                </Button>
                            </Right>
                    </Header>
                   
                </View>
                    );
            }else{
                return (
                    <View style={styles.container}>
                       
                        <Header>
                            <Left>
                                <Button transparent>
                                    <Icon name='menu' />
                                </Button>
                            </Left>
                            <Body>
                                <Title>{this.props.nameScreen.toUpperCase()}</Title>
                            </Body>
                            </Header>
                      
                    </View>);
            }
        }
                   
                    
               
        
    
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        height : 30,
        backgroundColor : '#4a4f6f'
    },
});

//make this component available to the app
export default HeaderApp;
