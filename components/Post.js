//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView, Image } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import BackgroundImage from './backgroundImage';
import  Input  from './common/Input';
import  Button  from './common/Button';
import PostItem from './PostItem';
import {APP_COLORS} from '../style/color';
// create a component
class Post extends Component {
    constructor (props) {
        super(props);
        this.state = {
            input: '',
            posts: [
                {
                    'textPost' : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas reiciendis temporibus nesciunt provident aliquid tenetur eum. Quis quaerat inventore ipsum nesciunt maiores labore architecto soluta fuga officia earum, quas id?",
                    'nameUser' : "Yacoub Eth",
                    'datepublish' : "18/11/2017"
                        
                },
                {
                   'textPost' : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas reiciendis temporibus nesciunt provident aliquid tenetur eum. Quis quaerat inventore ipsum nesciunt maiores labore architecto soluta fuga officia earum, quas id?",
                    'nameUser' : "Klaudia Bry",
                    'datepublish' : "Aujourd'hui"
                        
                } 
            ]
        };
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'POST',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
            }
        }
    }
    
    
    handleInput = (text) => {
        console.log(text);
        this.setState({input: text});
    }

    addComment = () =>{
        let oldposts= this.state.posts;
        let textConcurrent =  this.state.input;
        if(textConcurrent.trim().length >0){
            console.log('niveau 2');
            let newItem = {
                'textPost' : textConcurrent,
                 'nameUser' : "Klaudia Bry",
                 'datepublish' : "Aujourd'hui"
                     
             } ;   
            oldposts.push(newItem);
            this.setState({posts: oldposts,input: ""});
        }
        
    }

    displayPostItem =() =>{
        return this.state.posts.map((post, i) => {
            return (<PostItem key ={i} textPost={post.textPost} nameUser={post.nameUser} datepublish={post.datepublish} />)
            //return (<PostItem  textPost={'wwwwwwwwwwwwwwwwwwww'} nameUser={'Yacoub Eth'} datepublish={'19/11/2017'} />)
            
        })
    }
    render() {
        const {container, image, messages, input } = styles;
        return (
            <View style={styles.container}>
                <View style={styles.image}>
                    <BackgroundImage urlImage = {require('./icons/landscape.jpg')} >
                        <View style={styles.icons}>
                            <Image 
                            style= {styles.iconHeart} 
                            source= {require('./icons/heart.png')}   
                            />
                        </View>
                    </BackgroundImage>
                </View>

                <View style={styles.messages}>
                  <ScrollView>
                    {this.displayPostItem()}
                  </ScrollView>
                </View>

                <View style={styles.inputField}>
                    <View style={styles.input}>
                        <Input
                        onChangeText={(text) => this.handleInput(text)}
                        value={this.state.input}
                        placeholder="votre message"
                        />
                    </View>
                    <View style={styles.button}>
                        <Button
                        onPress={() => this.addComment()}
                        >COMMENTEZ</Button>
                    </View>
                </View>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    image: {
        flex: 5,
      
    },
    icons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        
    },
    iconHeart: {
        height: 10,
        width: 10,
    },

    messages: {
        flex: 9,
        backgroundColor: 'white',
    },
    inputField: {
        flex: 1,
        backgroundColor: '#ececf4',
        flexDirection: 'row',
        //justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        flex: 2,
        marginLeft: 5,
        marginRight: 5,
    },
    button: {
        flex: 1,
        marginRight: 5,
    },
    
});

//make this component available to the app
export default Post;
