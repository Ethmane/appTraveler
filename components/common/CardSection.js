import React from 'react';
import { View } from 'react-native';
import {APP_COLORS} from '../../style/color';


const CardSection = ({children, withBg}) => {
  return (
    <View style={ (withBg==true) ? [styles.containerStyle, {backgroundColor: '#fff'}] : [styles.containerStyle] }>
      {children} 
    </View>
  );
};

const styles = {
  containerStyle: {
    padding: 10,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: APP_COLORS.lines,
    position: 'relative',
  }
};

export {CardSection};


/*

containerStyle: {
    borderBottomWidth: 1,
    padding: 5,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    borderColor: '#ddd',
    position: 'relative',
    margin: 5,
  }
  */