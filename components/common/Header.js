//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

// create a component
const Header = (props) => {
   
        return (
            <View style={styles.viewStyle}>
                <Text style={styles.text}>{props.headerTitle}</Text>
            </View>
        );

}

// define your styles
const styles = StyleSheet.create({
    text: {
        fontSize: 20, 

    },
    viewStyle: {
        //backgroundColor: '#f8f8f8',
        backgroundColor: 'red',
        justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        paddingTop: 15,
        shadowColor: '#000',
        shadowOffset: {width: 0, height: 20},
        shadowOpacity: 0.2,
        elevation: 2,
        position: 'relative'
    }
});

//make this component available to the app
export  {Header};
