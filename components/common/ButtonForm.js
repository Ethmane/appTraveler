import React from 'react';
import { Text, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../../style/color';

const ButtonForm = ({ onPress, children, disabled, ref}) => {
  const { buttonStyle, textStyle, buttonDisabled, buttonEnabled} = styles;
//important  {children}
  return (
    <TouchableOpacity onPress={onPress} style={ (disabled) ? [buttonStyle,buttonDisabled] : [buttonStyle,buttonEnabled]} disabled={disabled}>
      <Text style={ (disabled) ? [textStyle,{color: APP_COLORS.secondaryText}] : [textStyle, {color: APP_COLORS.primary, borderColor: APP_COLORS.primary}]} >
        {children}
      </Text>
    </TouchableOpacity>
  );
};

const styles = {
  textStyle: {
    alignSelf: 'center',
    fontSize: 16,    
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  buttonStyle: {
    flex: 1,
    //important
    alignSelf: 'stretch',
    borderRadius: 5,
    borderWidth: 1,
  
  },
  buttonDisabled: {
    backgroundColor: APP_COLORS.primary,
    borderColor: APP_COLORS.secondaryText,
  },
  buttonEnabled: {
    backgroundColor: APP_COLORS.primaryAction,
    borderColor: APP_COLORS.primary,
  }
};
//important
export {ButtonForm};


