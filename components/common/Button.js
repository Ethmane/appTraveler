//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';

// create a component
const Button = ({onPress, children}) => {
    return (
        <TouchableOpacity onPress={ () => onPress()}>
            <View style={styles.container}>
                <Text  style={styles.btn}>{children}</Text>
            </View>
        </TouchableOpacity>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {

        backgroundColor: '#8285d6',
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn: {
        color: 'white',
        fontSize: 16,
        padding: 4,
    },

});

//make this component available to the app
export default Button;
