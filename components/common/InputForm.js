import React from 'react';
import { TextInput, View, Text } from 'react-native';
import {APP_COLORS} from '../../style/color';

const InputForm = ({ label, value, onChangeText, placeholder, secureTextEntry, onBlur, reverseContent }) => {
  const { inputStyle, labelStyle, containerStyle } = styles;
  
  if(reverseContent){
          return (
            <View style={containerStyle}>
            <TextInput
              secureTextEntry={secureTextEntry}
              placeholder={placeholder}
              autoCorrect={false}
              style={inputStyle}
              value={value}
              onChangeText={onChangeText}
              onBlur= {onBlur}
              autoCapitalize = 'none'
              underlineColorAndroid="rgba(0,0,0,0)"   
              autoCorrect={false}
            />
            <Text style={[labelStyle, {textAlign : 'right'}]}>{label}</Text>
            </View>
           )
      } else{
        return (
          <View style={containerStyle}>
          <Text style={labelStyle}>{label}</Text>
          <TextInput
            secureTextEntry={secureTextEntry}
            placeholder={placeholder}
            autoCorrect={false}
            style={inputStyle}
            value={value}
            onChangeText={onChangeText}
            onBlur= {onBlur}
            autoCapitalize = 'none'
            underlineColorAndroid="rgba(0,0,0,0)"   
            autoCorrect={false}
          /> 
          </View>)
      
    }
     
    
  
};

const styles = {
  containerStyle: { 
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center', 
    paddingHorizontal: '3%',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: APP_COLORS.secondaryText,  
    alignSelf: 'stretch',
    backgroundColor: APP_COLORS.primary,
  },
  inputStyle: {
    color:  APP_COLORS.primaryText,
    lineHeight: 18,
    flex: 7,
    fontSize: 14,    
    fontWeight: '600',
    paddingTop: 10,
    paddingBottom: 10
  },
  labelStyle: {   
    fontSize: 14,
    flex: 3,
  },
  
};

export { InputForm };