//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ActivityIndicator } from 'react-native';
import {APP_COLORS} from '../../style/color';

// create a component
const Spinner =  ({size, color, loading}) => {
    
        return (
            <View style={styles.container}>
                <ActivityIndicator
                size={size || 'large'}
                color={color|| APP_COLORS.primaryAction }
                animating={loading} 
                />
            </View>
        );

}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#2c3e50',
    },
});

//make this component available to the app
export {Spinner};
