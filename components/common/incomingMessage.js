//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Avatar } from 'react-native-elements';
import {APP_COLORS} from '../../style/color';
// create a component
const incomingMessage = ({message, user, date}) => {
    return (
        <View style={styles.container}>

            <View style={styles.avatar}>
                <Avatar
                medium
                rounded
                source={require('../icons/person.jpg')}
                onPress={() => console.log("Go to !")}
                activeOpacity={0.7}
                />
            </View>

            <View style={styles.message}>
                <View style={styles.messageTitle}>
                    <Text style={styles.messageTitle1}>{user}</Text>
                    
                    <Text style={styles.messageTitle2}> {date}</Text>
                </View>
                <View>
                    <Text style={styles.messageText}> {message} </Text>
                </View>
            </View>
            
        </View>
    );
};

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 25,
        flexDirection: 'row',
    },
    avatar: {
        flex: 1,
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    message: {
        flex: 4,
        marginRight: 20,
        
    },
    messageTitle: {
        flexDirection: 'row',
        marginBottom: 10,
    },
    messageTitle1: {
        fontWeight: 'bold',
        color: '#000',
        fontSize: 15,
    },
    messageTitle2: {
        color: '#6d6c71',
        fontSize: 15,
    },
    messageText: {
        paddingTop: 5,
        paddingLeft: 15,
        paddingBottom: 15,
        paddingRight: 15,
        backgroundColor: '#f5f6fb',
        borderRadius: 5,
        fontSize: 16,
        lineHeight: 30,
        color: '#6d6c71',
    },
    messageTime: {
        color: '#6d6c71',
        fontSize: 15,
        marginTop: 10,
        fontWeight: '100',
    },
});

//make this component available to the app
export  {incomingMessage};
