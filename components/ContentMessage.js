//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import {APP_COLORS} from '../style/color';
import { incomingMessage } from './common';
import  Input  from './common/Input';
import  Button  from './common/Button';

// create a component
class ContentMessage extends Component {

    constructor (props) {
        super(props);
        this.state = {
            input: '',
            messages: [
                /*{
                    'textMessage' : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas reiciendis temporibus nesciunt provident aliquid tenetur eum. Quis quaerat inventore ipsum nesciunt maiores labore architecto soluta fuga officia earum, quas id?",
                    'userName' : "Yacoub Eth",
                    'publishDate' : "18/11/2017"
                        
                },
                {
                    'textMessage' : "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas reiciendis temporibus nesciunt provident aliquid tenetur eum. Quis quaerat inventore ipsum nesciunt maiores labore architecto soluta fuga officia earum, quas id?",
                    'userName' : "Klaudia Bry",
                    'publishDate' : "Aujourd'hui"
                        
                } */

                
                   { id : 1, content : 'Salut bébé', src : 'out', date : '12/12/2017',  visible : true},
                   { id : 2, content : 'ca va', src : 'in' , date : '12/12/2017',  visible : true},
                   { id : 3, content : 'tu fais quoi ?', src : 'in', date : '12/12/2017',  visible : true},
                   { id : 4, content : 'je prepare les valises pour les vacances de Noel', src : 'out', date : '12/12/2017',  visible : true},
                   { id : 5, content : 'Ah Ok', src : 'in', date : '12/12/2017',  visible : true},
                   { id : 6, content : 'Tu viens ce soir ', src : 'out', date : '12/12/2017',  visible : true},
                   { id : 7, content : 'Biensûr  ', src : 'in', date : '12/12/2017',  visible : true},
                   { id : 8, content : 'on se voit au bar ? ', src : 'out', date : '12/12/2017',  visible : false},
                   { id : 9, content : '???? ', src : 'out', date : '12/12/2017',  visible : true}   
                   
            ]
        };
    }
    static navigationOptions =({navigation}) => {
        return {
            title: 'MESSAGE DE',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
        }
      }  
    }
    handleInput = (text) => {
        console.log(text);
        this.setState({input: text});
    }

    addMessage = () =>{
        let oldMessages = this.state.messages;
        let inputText =  this.state.input;
        if(inputText.trimreturn ().length > 0){
            console.log('niveau 2');
            let newMessage = {
                'textMessage' : inputText,
                'userName' : "Klaudia Bry",
                'publishDate' : "Aujourd'hui"       
             } ;   
            oldMessages.push(newMessage);
            this.setState({messages: oldMessages, input: ''});
        }
        
    }

    displayMessages =() =>{
         this.state.messages.map((message, i) => {
            return (<incomingMessage key ={i} message={message.textMessage} user={message.userName} date={message.publishDate} />)
            
            
        })
    }
render() {
    const {container, image, messages, input } = styles;
    return (
        <View style={styles.container}>
            <View style={styles.messages}>
                <ScrollView>
                {this.displayMessages()}
                </ScrollView>
            </View>
            <View style={styles.inputField}>
                <View style={styles.input}>
                    <Input
                    onChangeText={(text) => this.handleInput(text)}
                    value={this.state.input}
                    placeholder="votre message"
                    multiline
                    />
                </View>
                <View style={styles.button}>
                    <Button
                    onPress={() => this.addMessage()}
                    >ENVOYER</Button>
                </View>
            </View>
            
        </View>
    );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    image: {
        flex: 5,
      
    },
    icons: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        
    },
    iconHeart: {
        height: 10,
        width: 10,
    },

    messages: {
        flex: 9,
        backgroundColor: 'white',
    },
    inputField: {
        flex: 1,
        backgroundColor: '#ececf4',
        flexDirection: 'row',
        //justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        flex: 2,
        marginLeft: 5,
        marginRight: 5,
    },
    button: {
        flex: 1,
        marginRight: 5,
    },
});

//make this component available to the app
export default ContentMessage;
/*
            
*/