//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ScrollView } from 'react-native';
import { Container, Header, Content, SwipeRow, Button, List } from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import ItemMessage from './ItemMessage';
import {APP_COLORS} from '../style/color';

// create a component
class Messages extends Component {

    constructor(props){
        super(props);
        this.state = {
            messages : [
            {
                id : 1, name : 'Ethmane', surname :'Yacoub', iconName : 'person'
            },
            {
                id: 2, name : 'Brysiewicz', surname :' Klaudia', iconName : 'person'
            },
        ]
        }
    }

    componentWillMount() {
        // Appel Ajax pour alimenter le state.messages
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'MESSAGES',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
        }
      }  
    }
    
    callbackDelete = (id) => {

        this.deleteMessage(id)
    }

    deleteMessage = (id) => {
        var newMessages = this.state.messages.filter((el) => (el.id == id) ? false : true);
        this.setState({messages : newMessages}, ()=>{
            console.log("SUPPRESSION MESSAGE : Mise à jour BD");
            // Appel ajax à realiser
        })
    }

    displayMessage = () => {
        let  {navigation} =this.props;
        return this.state.messages.map((message, index) => {
             

            return(
                <ItemMessage key ={index} message ={message}  navigation={navigation}  callbackDelete={this.callbackDelete}/>  
            )
        });
    }


    render() {
        return (
            <Content scrollEnabled={true} style={styles.container}>
                    {this.displayMessage()}
            </Content>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        
    },
});

//make this component available to the app
export default Messages;
