//import liraries
import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Text,TouchableOpacity, Button, Alert  } from 'react-native';
import { Constants, Google } from 'expo';

import firebase from '../../../connexion/connexion';
//import {Icon} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import { ButtonForm, Card, CardSection, InputForm, Spinner  } from '../../common';
import {Authentification} from '../../../connexion';
import {APP_COLORS} from '../../../style/color';
import {I18n} from '../../../i18n/i18n';
import needReverseContent from '../../../config/languages';
import keys from '../../../connexion/keys';


// create a component
class Login extends Component {
    //important textinput must have taille
      
    state = {
    email: '',
    password: '',
    error: '',
    loading: false,
    disabled : true,
    }
    static navigationOptions =({navigation}) => {
        return {
            title: `${I18n.t('nameScreenLogin')}`,
            //headerLeft: null,
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),            
            headerTintColor: 'lightgrey',
            headerStyle: {
                backgroundColor:APP_COLORS.darkPrimary,
                borderWidth: 1,
                borderBottomColor: 'white',
            },
            headerTitleStyle: {
                alignSelf: 'center',
                //paddingRight: 55,
                color : 'white'
             },
            drawerLabel: () => null
        }
    }
    
      handleChangePassword = () =>{
        let { email, password} = this.state;
        if(password.length > 3){
            this.handleValidateForm(true);
        }
      }
      handleValidateForm = (bool =false) => {
        let { email, password} = this.state;
 
        let contentTextError= '';
 
         
         if( email !='' && !(/^[^\W][-a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/.test(email)) ){
             //contentTextError += `Email < ${email} > : Format invalide  \n`;
             contentTextError += `${I18n.t('msg_error_mail')}`; 
             this.setState({email: ''});
         }
  
         if (bool && password.trim() == ''){
             contentTextError += `${I18n.t('msg_error_empty')}`;
             console.log('Non identiques');
         }
 
         this.setState({error: contentTextError});
 
        condition =  (email != '') && (password != '');
        if(condition){
             this.setState({disabled: false});
        }else{
         this.setState({disabled: true});
        }
             
    } 
    onButtonPress =() =>  {
            const {email, password} = this.state;
            this.setState({error:'',loading: true})
            Authentification(email, password).then(() => this.onLoginSucess() ).catch(() => this.onLoginFail());
            
    }
    onLoginSucess = () => {
        this.setState({loading: false, error: ''});

        let {emailVerified} = firebase.auth().currentUser;
        if(emailVerified){
            this.props.navigation.navigate('ViewTimeline');
        }else{
            this.props.navigation.navigate('ViewWaitingConfirmation');
        }
        
    }
    onLoginFail =() =>{
        //this.setState({error: "Echec d'autentification", loading: false})
        this.setState({error: `${I18n.t('msg_error_auth')}`, loading: false})
    }
    renderButton(){
        if(this.state.loading){
            return(
                <Spinner
                loading={this.state.loading}
                />
            );
        };
        return(
            <ButtonForm disabled={this.state.disabled}
            onPress={ () => this.onButtonPress()}
            >
            {I18n.t('btn_login')}
            </ButtonForm>
        );         
    }

    signIn = async () => {
        try {
            const { type, user,idToken, accessToken } = await Google.logInAsync({
              androidStandaloneAppClientId: '<ANDROID_CLIENT_ID>',
              iosStandaloneAppClientId: '<IOS_CLIENT_ID>',
              androidClientId: '564000833313-q42oiamd61oskpcjlomp62ch355lqlbr.apps.googleusercontent.com',
              iosClientId: '564000833313-hucf5fmp6lb1en3kaf1f2t76ml0rhgqt.apps.googleusercontent.com',
              scopes: ['profile', 'email']
            });
      
            switch (type) {
              case 'success': {
                Alert.alert(
                  'Logged in!',
                  `Hi ${user.name}!`,
                );
                console.log(user);
                console.log(type);
                console.log(idToken);
                console.log(accessToken);
                
                const credential = firebase.auth.GoogleAuthProvider.credential(idToken, accessToken);
                
                firebase.auth().signInWithCredential(credential).catch((error) => {
                  // Handle Errors here.
                  console.log("Error authenticating with Google");
                  console.log(error);
                  console.log(error.message);
                });
      
                break;
              }
              case 'cancel': {
                Alert.alert(
                  'Cancelled!',
                  'Login was cancelled!',
                );
                break;
              }
              default: {
                Alert.alert(
                  'Oops!',
                  'Login failed!',
                );
              }
            }
          } catch (e) {
            Alert.alert(
              'Oops!',
              'Login failed!',
            );
          }
    }

    render() {
        const {container, textContainer, text, errorTextStyle, importantText}  = styles;
        let {email} = this.state;

        return (
            <View style={container}>
            <Card>
               <CardSection>
                    <InputForm
                        reverseContent = {needReverseContent(I18n.locale)}
                        placeholder="user@gmail.com"
                        label= {`${I18n.t('lb_mail')}`}
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                        onBlur= {() => this.handleValidateForm()}   
                    />
                </CardSection>
                <CardSection>
                    <InputForm
                    reverseContent = {needReverseContent(I18n.locale)}
                    placeholder={`${I18n.t('lb_password')}`}
                    label={`${I18n.t('lb_password')}`}
                    value={this.state.password}

                    onChangeText={(password) => {this.setState({ password }, ()=>{
                        this.handleChangePassword();
                    }) }}

                    onBlur= {() => this.handleValidateForm()}
                    secureTextEntry
                    />
                </CardSection>
                <View style={textContainer}>
                    <Text style={errorTextStyle}>
                        {this.state.error}
                    </Text>
                </View>
                <CardSection>
                    {this.renderButton()}      
                </CardSection>
                <TouchableOpacity   style={textContainer} onPress={() => this.props.navigation.navigate('viewSignUp') }>
                    <Text style={text}>
                        <Text style={importantText}> {I18n.t('lb_create_account')}</Text>
                    </Text> 
                </TouchableOpacity>
                <TouchableOpacity   style={textContainer} onPress={() => this.props.navigation.navigate('viewForgetPassword', { email }) }>
                    <Text style={text}>
                        <Text style={importantText}> {I18n.t('lb_forget_id')}</Text>
                    </Text> 
                </TouchableOpacity>
            </Card>
                <CardSection>
                    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', paddingTop: Constants.statusBarHeight, backgroundColor: '#ecf0f1',}}>
                    <Button
                        title="Login with Google"
                        onPress={this.signIn}/>
                    </View>
                </CardSection>
            </View>
        );     
    }
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    }, 
    textContainer: {
        paddingLeft: 8,
        margin: 5,
    },
    errorTextStyle : {
        fontSize: 16,
        alignSelf: 'center',
        color : APP_COLORS.accent,
    },
    text: {
        color : APP_COLORS.secondaryText
    },
    importantText: {
        color : APP_COLORS.primaryAction
    },
    header: {
        fontSize: 25
      },
});

//make this component available to the app
export default Login;
