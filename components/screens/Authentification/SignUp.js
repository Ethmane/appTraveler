//
import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Text,TouchableOpacity } from 'react-native';
import firebase from '../../../connexion/connexion';
import {Icon} from 'native-base';
import { ButtonForm, Card, CardSection, InputForm, Spinner, Header } from '../../common';
import {Inscription} from '../../../connexion';
import {APP_COLORS} from '../../../style/color';
import {I18n} from '../../../i18n/i18n';
import needReverseContent from '../../../config/languages';

// cree un composant for inscription
class SignUp extends Component {
    //important textinput must have taille
      
    state = {
        prenom : '',
        nom : '',
        numerophone : '',
        email: '',
        password: '',
        passwordConfirm : '',
        error: '',
        loading: false,
        disabled : true,
    }
    static navigationOptions =({navigation}) => {
        return {
            title: `${I18n.t('nameScreenSignUp')}`,
            //headerLeft: (<Icon name="menu" size={35} style={{color: "#FFF"}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerLeft : null,
            headerTintColor: 'lightgrey',
            headerStyle: {
                backgroundColor: APP_COLORS.darkPrimary,
                borderWidth: 1,
                borderBottomColor: 'white',
             },
             headerTitleStyle: {
                alignSelf: 'center',
                //paddingRight: 55,
                color : 'white'   
             },
             drawerLabel: () => null
        }
    }
    
    onButtonPress =() => {
        const {email, password} = this.state;
        this.setState({error:'',loading: true});
        Inscription(email, password).then(() => this.onLoginSucess())
                   .catch((error) => this.onLoginFail(error)
                );
            
    }
    onLoginSucess = () => {
        let user = firebase.auth().currentUser;
        user.sendEmailVerification().then(function() {
            // Email sent.
            this.setState({email: '', password: '', loading: false, error: ''});
            this.props.navigation.navigate('ViewWaitingConfirmation')
          }).catch(function(error) {
            // An error happened.
            this.setState({email: '', password: '', loading: false, error: `Erreur lors d'envoi de mail de confirmation` });
            /** Une modale à priviligier  pour afficher le message => redirection une fois le btn est pressé vers login*/
        });
        
    }
    onLoginFail = (error) => {
        let textError="";
        switch(error){
            case "auth/email-already-in-use" : 
                textError = `${I18n.t('_msg_error_type1')}` 
                break;
            case "auth/invalid-email" : 
                textError = `${I18n.t('_msg_error_type2')}` 
                break;
            case "auth/operation-not-allowed" : 
                textError = `${I18n.t('_msg_error_type3')}` 
                break;
            case "auth/weak-password" : 
                textError = `${I18n.t('_msg_error_type4')}` 
                break;                                   
        }
        this.setState({error: textError, loading: false})
    }


   handleValidateForm = () => {
       let { prenom, nom , numerophone, email, password, passwordConfirm } = this.state;

       let contentTextError= '';

        if( email !='' && !(/^[^\W][-a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/.test(email)) ){
            //contentTextError += `Email < ${email} > : Format invalide  \n`;
            contentTextError += `${I18n.t('msg_error_mail')}` ;
            this.setState({email: ''});
        }

        if( password != '' && !(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(password))){
            //contentTextError += `Mot de Passe < ${password} > : Format invalide 
            contentTextError += `${I18n.t('_msg_error_password_format')}` ;
            this.setState({password: ''});
        }

        if( passwordConfirm != '' &&  !(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/.test(passwordConfirm))){
            //contentTextError += `Mot de Passe Confirmation < ${password} > : Format invalide `;
            contentTextError += `${I18n.t('_msg_error_password_confirm')}` ;
            this.setState({passwordConfirm: ''});
        }

        // cond1 && cond2
        // Confirmation password (Regex à ajouter)
        if (password != '' && passwordConfirm != '' && passwordConfirm != password){
            this.setState({password: ''});
            this.setState({passwordConfirm: ''});
            //contentTextError += "Mots de passes non identiques";
            contentTextError += `${I18n.t('_msg_error_password_no_identic')}`;
            console.log('Non identiques');
        }

        this.setState({error: contentTextError, loading: false});

       //condition = (prenom != '') && (nom != '') && (numerophone != '') && (email != '') && (password != '') && (passwordConfirm != '')  && (passwordConfirm == password);
       condition = (email != '') && (password != '') && (passwordConfirm != '')  && (passwordConfirm == password);
       if(condition){
            this.setState({disabled: false});
       }else{
        this.setState({disabled: true});
       }
            
   } 
 
    renderButton =() => {
        if(this.state.loading){
            return(
                <Spinner
                color='red'
                loading={this.state.loading}
                />
            );
        } 
        return(
            <ButtonForm disabled= {this.state.disabled} 
            onPress={ () => this.onButtonPress()}
            >
             {I18n.t('_btn_signup')}
            </ButtonForm>
        );
    }
    
    render() {

        const {container, textContainer, text, errorTextStyle, importantText}  = styles;
        return (
            <View style={container}>
                <Card>
                    <CardSection>
                        <InputForm
                        reverseContent = {needReverseContent(I18n.locale)}
                        placeholder="user@gmail.com"
                        label={`${I18n.t('lb_mail')}`}
                        value={this.state.email}
                        onChangeText={email => this.setState({ email })}
                        onBlur= {() => this.handleValidateForm()}
                        />
                    </CardSection>
                    <CardSection>
                        <InputForm
                        reverseContent = {needReverseContent(I18n.locale)}
                        placeholder={I18n.t('lb_password')}
                        label={I18n.t('lb_password')}
                        value={this.state.password}
                        onChangeText={password => this.setState({ password })} onBlur= {() => this.handleValidateForm()}
                        secureTextEntry
                        />
                    </CardSection>
                    <CardSection>
                        <InputForm
                        reverseContent = {needReverseContent(I18n.locale)}
                        placeholder={I18n.t('_txt_password_confirm')}     
                        label={I18n.t('lb_password')}
                        value={this.state.passwordConfirm}
                        onChangeText={ (passwordConfirm) => this.setState({ passwordConfirm }, () =>{
                            if(passwordConfirm.length === this.state.password.length) this.handleValidateForm();
                        })}
                        onBlur= {() => this.handleValidateForm()}
                        secureTextEntry
                        />
                    </CardSection>
                    <View style={textContainer}>
                        <Text style={errorTextStyle}>
                            {this.state.error}
                        </Text>
                    </View>  
                    <CardSection>
                        {this.renderButton()}
                    </CardSection>
                    <TouchableOpacity style={textContainer} onPress={() => this.props.navigation.navigate('viewLogin') }>
                        <Text style={text}>
                            <Text style={importantText}> {I18n.t('_lb_have_ready_account')} </Text>
                        </Text> 
                    </TouchableOpacity>        
                </Card>
            </View>
        );
    }
}



const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    }, 
    textContainer: {
        paddingLeft: 8,
        margin: 5,
    },
    errorTextStyle : {
        fontSize: 16,
        alignSelf: 'center',
        color : APP_COLORS.accent,
    },
    text: {
        color : APP_COLORS.secondaryText
    },
    importantText: {
    color : APP_COLORS.primaryAction
    }
});
//make this component available to the app
export default SignUp;
