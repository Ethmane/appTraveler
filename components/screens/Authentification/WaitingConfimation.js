//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import {APP_COLORS} from '../../../style/color';
import { NavigationActions } from 'react-navigation';
import firebase from '../../../connexion/connexion';
// create a component
class WaitingConfirmation extends Component {
       constructor(props){
           super(props);
           state = {
            emailVerified : false
           }
       }

       static navigationOptions =({navigation}) => {
        return {
            title: 'ATTENTE DE CONFIRMATION',
            headerLeft: null,
            headerTintColor: 'lightgrey',
            headerStyle: {
                backgroundColor:  APP_COLORS.darkPrimary,    
             },
             headerTitleStyle: {
                alignSelf: 'center',
                //paddingRight: 55,
                color : 'white'
             },
             drawerLabel: () => null
        }
      }
     componentDidMount() {
        let {emailVerified} = firebase.auth().currentUser
        this.setState({emailVerified});   
     }

     sendEmailVerification =()=>{
        let user = firebase.auth().currentUser;
        user.sendEmailVerification().then(function() {
            // Email sent.
          }).catch(function(error) {
            // An error happened.
           
        });
     }
    render() {
        const {container, importantText, text}  = styles;
        let {emailVerified} = this.state;

        return (
            <View style={container}>
                <Text style={text} >
                  {(emailVerified) ?  ` Votre Compte est en attente de confirmation` : `Veuillez confirmer votre mail via votre messagerie`}
                 </Text>

                 {!emailVerified &&
                  <TouchableOpacity  onPress={() => sendEmailVerification() }>
                    <Text style={text}>
                        Pour ré-envoyer le mail de confirmation : <Text style={importantText}> Cliquez ici.</Text>   
                    </Text> 
                   </TouchableOpacity>
                 }

                <TouchableOpacity  onPress={() => this.props.navigation.navigate('viewLogin') }>
                <Text style={text}>
                    Pour se deconnecter: <Text style={importantText}> Cliquez ici.</Text>   
                </Text> 
                </TouchableOpacity>
            </View>
        );
    }   
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        paddingLeft: '5%',     
        backgroundColor: APP_COLORS.primary,
    },
    text: {
        lineHeight: 25,
        fontSize: 15,
    },   
    importantText: {
        color : APP_COLORS.primaryAction,
        //fontWeight: 500,
        fontSize: 18,
   },    
});

//make this component available to the app
export default WaitingConfirmation;

/*
 <Text style={{fontSize : 18, alignSelf : 'center',  justifyContent: 'center', fontWeight: 'bold'}}>Votre Compte est en attente de confirmation,
                 Veuillez patienter</Text>
                <TouchableOpacity style={{alignSelf : 'center'}} onPress={() => this.props.navigation.navigate('viewLogin') }>
                <Text style={{fontStyle: 'italic',fontFamily: 'Verdana'}}>
                     Pour se deconnecter, <Text style={{color : 'blue'}}> Cliquez ici !!</Text>
               </Text> 
                </TouchableOpacity>
                */
