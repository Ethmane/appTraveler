  
  //import liraries
import React, { Component } from 'react';
import { View, StyleSheet, TextInput, Text,TouchableOpacity, Image } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import { Icon as MyIcon} from 'react-native-elements'
import Modal from 'react-native-modal';

import { ButtonForm, Card, CardSection, InputForm, Spinner  } from '../../common';
import {GetPasswordByEmail} from '../../../connexion';
import {APP_COLORS} from '../../../style/color';
import {I18n} from '../../../i18n/i18n';
import needReverseContent from '../../../config/languages';
// create a component

class ForgetPassword extends Component {
    //important textinput must have taille
      
    state = {
    email: '',
    error: '',
    loading: false,
    disabled : true,
    isSucces: false
    }

    componentDidMount(){
        const { navigation } = this.props;
        let email = navigation.getParam('email', '');
        if(email != '') this.setState({email, disabled : false});
    }

    static navigationOptions =({navigation}) => {
        return {
            title: `${I18n.t('nameScreenForgetPassword')}`,
            //headerLeft: null,
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('ViewLogin') } />),            
            headerTintColor: 'lightgrey',
            headerStyle: {
                backgroundColor:APP_COLORS.darkPrimary,
                borderWidth: 1,
                borderBottomColor: 'white',
            },
            headerTitleStyle: {
                alignSelf: 'center',
                //paddingRight: 55,
                color : 'white'
             },
            drawerLabel: () => null
        }
    }
    
      handleChangePassword = () =>{
        let { email} = this.state;
        if(password.length > 3){
            this.handleValidateForm(true);
        }
      }
      handleValidateForm = (bool =false) => {
        let { email} = this.state;
 
        let contentTextError= '';
 
         
         if( email !='' && !(/^[^\W][-a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\@[a-zA-Z0-9_]+(\.[a-zA-Z0-9_]+)*\.[a-zA-Z]{2,4}$/.test(email)) ){
             //contentTextError += `Email < ${email} > : Format invalide  \n`;
             contentTextError += `${I18n.t('msg_error_mail')}`; 
             this.setState({email: ''});
         }
  
         
         this.setState({error: contentTextError});
 
        condition =  (email != '') ;
        if(condition){
             this.setState({disabled: false});
        }else{
         this.setState({disabled: true});
        }
             
    } 
    onButtonPress =() =>  {
            const {email} = this.state;
            this.setState({error:'',loading: true})
            GetPasswordByEmail(email).then(() => this.onLoginSucess() ).catch(() => this.onLoginFail());
    }

    onLoginSucess = () => {
        this.setState({loading: false, error: ``, disabled : true,isSucces : true });
    }

    onLoginFail =() =>{
        this.setState({error: `${I18n.t('msg_error_unknown_email')}`, loading: false})
    }
    
    renderButton(){
        if(this.state.loading){
            return(
                <Spinner
                loading={this.state.loading}
                />
            );
        };
        return(
            <ButtonForm disabled={this.state.disabled}
            onPress={ () => this.onButtonPress() }
            >
            {I18n.t('btn_send')}
            </ButtonForm>
        );         
    }

    _renderButton = (text, onPress) => (
        <TouchableOpacity onPress={onPress}>
          <View style={styles.button}>
            <Text>{text}</Text>
          </View>
        </TouchableOpacity>
      );
    

    _renderModalContent = () => {
        
        let {isSucces} = this.state;
        

        if(isSucces){
            return (<View style={styles.modalContent}>
                        <Text> <Image style={{width : 50, height : 50 }} source={require("../../icons/mail-send.png")} /> Veuillez consulter votre messagerie </Text>
                        {this._renderButton('Fermer', () => {
                            this.setState({ isSucces: false });
                            this.props.navigation.navigate('ViewLogin');
                            })
                        }
                    </View>)
        }else
            return null;
    

    }

    render() {
        

        const {container, textContainer, text, errorTextStyle, importantText}  = styles;
        let {isModalVisible, isSucces, email} = this.state;
            return (
                <View style={container}>
                    <Card>
                    <CardSection>
                            <InputForm
                                reverseContent = {needReverseContent(I18n.locale)}
                                placeholder="user@gmail.com"
                                label= {`${I18n.t('lb_mail')}`}
                                value={this.state.email}
                                onChangeText={email => this.setState({ email })}
                                onBlur= {() => this.handleValidateForm()}   
                            />
                    </CardSection>
                        
                    <View style={textContainer}>
                        <Text style={errorTextStyle}>
                            {this.state.error}
                        </Text>
                    </View>
                    <CardSection>
                        {this.renderButton()}      
                    </CardSection>      
                </Card>
                <Modal isVisible={isSucces}
                        animationIn={'slideInLeft'}
                        animationOut={'slideOutRight'}
                        animationInTiming={500}
                        animationOutTiming={500}
                        backdropTransitionInTiming={500}
                        backdropTransitionOutTiming={500}
                >
                    {this._renderModalContent()}
                </Modal>
               
            </View>
        );
        

    }
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
    }, 
    textContainer: {
        paddingLeft: 8,
        margin: 5,
    },
    errorTextStyle : {
        fontSize: 16,
        alignSelf: 'center',
        color : APP_COLORS.accent,
    },
    text: {
        color : APP_COLORS.secondaryText
    },
    importantText: {
        color : APP_COLORS.primaryAction
    },
    modalContent: {
        backgroundColor: 'white',
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)'
      },
      button: {
        backgroundColor: 'lightblue',
        padding: 8,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      }
});

//make this component available to the app
export default ForgetPassword;
