//import liraries
import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView, TextInput, TouchableOpacity, Text as TextNative} from 'react-native';
import {StackNavigator} from 'react-navigation';
import { Avatar } from 'react-native-elements'
import {Container,Content, Form, Item, Input, Label, H1, H2, H3, Text, Right, List, ListItem, Left, Body, Container as ContentContainer }  from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Switch from 'react-native-switch-pro';
import Traveler from './Traveler';
import {APP_COLORS} from '../../../style/color';

// create a component 
class ProfileContact extends Component {
    constructor(props){
        super(props);
        this.state = {
            profil: {
                userName : 'Yacoub',
                userSurName :'Ethmane',   
                userNumberPhone : '070089768',
                urlFacebook: 'Yaac',
                urlTwitter : ''
            }
        };
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'PROFILE CONTACT',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () =>  navigation.navigate('ViewTraveler') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
             }
        }
      }  

        render() {   
        return (
            <View style ={styles.container}>

                    <View style= {styles.imageContainer}>
                        <Image  style ={styles.imageFond} source = {require('../../icons/landscape.jpg')} >
                                <Avatar
                                large
                                rounded
                                source={require('../../icons/person.jpg')}
                                activeOpacity={0.7}
                                />
                        </Image>  
                    </View>

                    <Content style={{flex : 10}}>

                            <View >
                                <View style={[{flex : 1}, styles.titleView]}>
                                    <H3 style={styles.titleText}> DONNEES PERSONNELLES</H3>
                                </View>
                                
                                <Item  stackedLabel style={styles.itemContainer}>
                                    <Label >  <Text style={styles.labelText}>Nom de famille</Text></Label>
                                    <Input  editable = {false} value={this.state.profil.userName}/>
                                </Item>    
                                
                                <Item  stackedLabel style={styles.itemContainer}>
                                    <Label >  <Text style={styles.labelText}>Prenom</Text></Label>
                                    <Input   editable = {false} value={this.state.profil.userSurName}/>
                                </Item>
                                
                                <Item  stackedLabel style={styles.itemContainer} >
                                    <Label >  <Text style={styles.labelText}>Numero de Téléphone</Text></Label>  
                                    <Input  editable = {false} value={this.state.profil.userNumberPhone}/>
                                </Item>
                            </View>

                            <View >
                                <View style={[{flex : 1}, styles.titleView]}>
                                    <H3 style={styles.titleText}>RESEAUX SOCIAUX</H3>
                                </View>

                                <Item  stackedLabel style={styles.itemContainer}>
                                    <Label > <Text style={styles.labelText}>Facebook</Text> </Label>
                                    <Input   editable = {false} value={this.state.profil.urlFacebook}/>
                            
                                </Item>

                                <Item  stackedLabel style={styles.itemContainer}>
                                    <Label > <Text style={styles.labelText}>Twitter</Text></Label>
                                    <Input   editable = {false} value={this.state.profil.urlTwitter}/>
                                </Item>
                            </View>
                               
                    </Content>
          
            </View>
        );
    }
}

// define  styles
const styles = StyleSheet.create({
    container : {
        flex: 1,       
    },
    header : {
        flex: 1,
    },
    imageContainer: {
        flex: 0.7, 
        backgroundColor: 'transparent',
        marginTop: 5,
    },
    imageFond : {
        flex: 1,
        position : 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width : '100%',
        height : 250,
    },
    titleView :{
        backgroundColor: '#f1f1f1',
        paddingTop: '5%',
        paddingLeft: '5%',
        paddingBottom: '1%'
    },
    titleText :{
        fontSize: 20,
        color: 'black',  
    },
    itemContainer: {
        marginLeft: '5%',
        marginRight: '5%',
    },
    labelText :{
        fontSize: 15,
        color: 'gray',  
    },
    
  
});

//make this component available to the app
export default ProfileContact;
/*

 */