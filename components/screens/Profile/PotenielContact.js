//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import {I18n} from '../../../i18n/i18n';
import { Modal } from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';

const images = [{
    // Simplest usage.
    url: 'https://avatars2.githubusercontent.com/u/7970947?v=3&s=460',

    // width: number
    // height: number
    // Optional, if you know the image size, you can set the optimization performance

    // You can pass props to <Image />.
    props: {
        // headers: ...
    }
}, {
    url :"https://images.pexels.com/photos/248261/pexels-photo-248261.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb",
    props: {
      source: {
        uri:
          "https://images.pexels.com/photos/248261/pexels-photo-248261.jpeg?w=1260&h=750&auto=compress&cs=tinysrgb"
      }
    }
}]


  
// create a component
class PotenielFriend extends Component {

    static navigationOptions =({navigation}) => {
        return {
            title: 'Potentiel Friend',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
            }
        }
    }

    state = {
      isVisible : true
    }

    visibleModal = (_bool) => {
        this.setState({isVisible : _bool});
    }

    render() {

        //console.log("=> ", I18n.t('greeting'));

        return (
            <View style={styles.container}>
              <View>
                <Text>We are the world</Text>
              </View>
              <Modal visible={this.state.isVisible} transparent={true} onRequestClose={() => { this.visibleModal(false); } }>
                <ImageViewer imageUrls={images} enableSwipeDown={true} />
              </Modal>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: '#2c3e50',
        backgroundColor: '#fff',
    },
});

//make this component available to the app
export default PotenielFriend;
