//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Modal, TouchableWithoutFeedback} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';

import {StackNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/EvilIcons';
import BackgroundImage from '../../backgroundImage';
import {APP_COLORS} from '../../../style/color';
import ProfileContact from './ProfileContact';
import ContentMessage  from '../../ContentMessage';


const ProfileContactStack = StackNavigator({
    viewProfileContact : {
      screen : ProfileContact
    }
  });

const ContentMessageStack = StackNavigator({
    ViewContentMessage : {
        screen : ContentMessage,
    },
});

const images = [
    { props :{
        source : require('../../icons/egypte.jpeg') }
    },
    { props :{
        source : require('../../icons/greece.jpeg') }
    },
    { props :{
        source : require('../../icons/beach.jpeg') }
    },
    { props :{
        source : require('../../icons/iceland.jpeg') }
    },
    { props :{
        source : require('../../icons/desert.jpeg') }
    },
];

// create a component
class Traveler extends Component {

    constructor(props){
        super(props);
        this.state = {
            ImagesCollection : [],
            UrlImageBackground : '',
            urlImageProfil : '',
            firstNameContact : '',
            nameContact : '',
            descContact : '',
            companyContact : '',
            isModalOpened: false, 
            currentImageIndex: 0
        }
    }

    
    static navigationOptions =({navigation}) => {
        return {
            title: 'TRAVELER',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
             }
        }
      }

    openModal =(index) => {
        this.setState({isModalOpened: true, currentImageIndex: index })
    }
    visibleModal =() =>{
        this.setState({isModalOpened : false})
    }
    
    render() {
        /**
         * Valeurs stockées en dur
         */
        let firstNameContact = "Yacoub",
         nameContact = "Ethmane",
         descContact = "Je suis un ingénieur en développement Web",
         companyContact = "BNP_Parisbas"; 

        /**
         * acces aux styles
         */
        const {container,imageFond, profil,
            profilSup,profilSupOne, profilSupTwo, profilSupThree, 
            profilInf,profilInfText1, profilInfText2, profilInfText3,
            indicator,indicatorContent1, indicatorContent2, indicatorContent3,indicatorContentText2, indicatorContentText1,
            groupImage, groupImageSection1,groupImageSection2,groupImageSection1Item,groupImageSection2Item,
            groupImageSyle
            } = styles;  
        


        return (
            <View style={container}>
            
                <View style= {profil}>
                    <BackgroundImage urlImage = {require('../../icons/landscape.jpg')} isOverlay={true} >
                        <View style= {profilSup}>
                                <View style= {profilSupOne}>  
                                        <Avatar
                                            size="small"
                                            rounded
                                            source={require('../../icons/plus.png')}
                                            onPress={() => {
                                                console.log(this.props); this.props.navigation.navigate('viewProfileContact')
                                            }}
                                            activeOpacity={0.7} 
                                            />
                                            
                                </View>
                                <View style= {profilSupTwo}>
                                        <Avatar
                                            size="large"
                                            rounded
                                            source={require('../../icons/person.jpg')}
                                            onPress={() =>{}}
                                            activeOpacity={0.7}
                                            />
                                </View>
                                <View style= {profilSupThree}>
                                        <Avatar
                                            size="small"
                                            rounded
                                            source={require('../../icons/iconmessage.png')}
                                            onPress={() => {console.log(this.props); this.props.navigation.navigate('ViewContentMessage')}}
                                            activeOpacity={0.7}
                                            />
                                </View>
                        </View>

                        <View style= {profilInf}>
                            <Text style= {profilInfText1}> {firstNameContact + " "+ nameContact} </Text>
                            <Text style= {profilInfText2}> {descContact}</Text>
                            <Text style= {profilInfText3}> work at @{companyContact}</Text>
                        </View>
                     </BackgroundImage>
                 </View>
            
               <View style= {indicator}>
                    <View style= {indicatorContent1}>
                            <Text style= {indicatorContentText1}> 655 </Text>
                            <Text style= {indicatorContentText2}> Followers </Text>
                    </View>
                    <View style= {indicatorContent2}>
                            <Text style= {indicatorContentText1}> 45 </Text>
                            <Text style= {indicatorContentText2}> Pictures </Text>
                    </View>
                    <View style= {indicatorContent3}>
                            <Text style= {indicatorContentText1}> 243 </Text>
                            <Text style= {indicatorContentText2}> Following </Text>
                    </View>
               </View>

               <View style= {groupImage}>
                    <View style= {groupImageSection1}> 

                        <View style={groupImageSection1Item} key={0}>
                            <TouchableWithoutFeedback onPress={() => {this.openModal(0)}}>
                                <Image
                                    resizeMode="cover"
                                    style = {groupImageSyle} source={require('../../icons/egypte.jpeg')}
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={groupImageSection1Item} key={1}>
                            <TouchableWithoutFeedback onPress={() => {this.openModal(1)}}>
                                <Image
                                    resizeMode="cover"
                                    style = {groupImageSyle} source={require('../../icons/greece.jpeg')}
                                />
                            </TouchableWithoutFeedback>
                        </View>            
                    </View>

                    <View style= {groupImageSection2}> 

                    <View style={groupImageSection2Item} key={2}>
                        <TouchableWithoutFeedback onPress={() => {this.openModal(2)}}>
                            <Image
                                resizeMode="cover"
                                style = {groupImageSyle} source={require('../../icons/beach.jpeg')}
                            />
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={groupImageSection2Item} key={3}>
                        <TouchableWithoutFeedback onPress={() => {this.openModal(3)}}>
                            <Image
                                resizeMode="cover"
                                style = {groupImageSyle} source={require('../../icons/iceland.jpeg')}
                            />
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={groupImageSection2Item} key={4}>
                        <TouchableWithoutFeedback onPress={() => {this.openModal(4)}}>
                            <Image
                                resizeMode="cover"
                                style = {groupImageSyle} source={require('../../icons/desert.jpeg')}
                            />
                        </TouchableWithoutFeedback>
                    </View>
                        
                    </View>
               </View>

               <Modal visible={this.state.isModalOpened} transparent={true} onRequestClose={() => { this.visibleModal()}}>
                    <ImageViewer imageUrls={images} index={this.state.currentImageIndex}/>
               </Modal>
            </View>
        );
    }   
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,  
    },
    imageFond : {
        position : 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width : '100%',
        height : '100%',

    },
    profil : {
        flex: 7, 
        width : '100%',
        height : '100%',
        //flexDirection : 'column'
    },
    profilSup:{
        flex: 13,
        flexDirection : 'row'
    },
    profilSupOne: {
        flex : 1,
        width : '100%',
        alignSelf : 'center',
        alignItems : 'flex-end'
    },
    profilSupTwo: {
        flex : 1,
        width : '100%',   
        alignSelf : 'center',
        alignItems :'center'
    },
    profilSupThree: {
        flex : 1,
        width : '100%',
        alignSelf : 'center'
    },
    profilInf: {
        flex : 7,
        alignSelf : 'center'
        
    },

    profilInfText1 : {
        fontWeight : 'bold',
        fontSize : 18,
        color : 'white',
        alignSelf : 'center'
    },
    profilInfText2:{
        marginTop : 2,   
        color : 'white',
        alignSelf : 'center'
        
   },
    profilInfText3 :{
        fontWeight : 'bold',
        color : 'white',
        alignSelf : 'center'       
     },
    indicator : {
        marginTop: 20,
        flex: 2,
        flexDirection : 'row',
        alignSelf :'center'
    },
    indicatorContent1 : {
        flex: 1,
        alignSelf :'center',
        borderRightColor: 'gray',
        borderRightWidth: 1

    },
    indicatorContent2 : {
        flex: 1, 
        alignSelf :'center',
        borderRightColor: 'gray',
        borderRightWidth: 1
    },
    indicatorContent3 : {
        flex: 1,
        alignSelf :'center'
    },
    indicatorContentText1 :{
        alignSelf :'center',
        fontSize : 18,
        fontWeight : 'bold'

    },
    indicatorContentText2: {
        alignSelf :'center',
        color : 'gray',
        opacity : 0.6   
    },

    groupImage : {
        flex: 8, 
    },
    groupImageSection1 : {
        flex : 3,
        flexDirection : 'row'
    },
    groupImageSection1Item :{
        flex : 1
    }, 
    groupImageSection2 : {
        flex : 2,
        flexDirection : 'row'
    },
    groupImageSection2Item :{
        flex : 1
    },
    groupImageSyle : {
        width : '100%',
        height : '100%',  
    }
});

//make this component available to the app
export default Traveler;