//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Button } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Modal as ModalRN, TouchableWithoutFeedback, TouchableOpacity} from 'react-native';
import ImageViewer from 'react-native-image-zoom-viewer';
import Modal from 'react-native-modal';
import {StackNavigator} from 'react-navigation';
import Icon from 'react-native-vector-icons/EvilIcons';
import { ImagePicker } from 'expo';

import BackgroundImage from '../../backgroundImage';
import {APP_COLORS} from '../../../style/color';
import ProfileContact from './ProfileContact';
import ContentMessage  from '../../ContentMessage';
import needReverseContent from '../../../config/languages';
import {I18n} from '../../../i18n/i18n';
import { ButtonForm, Card, CardSection, InputForm, Spinner  } from '../../common';


const ProfileContactStack = StackNavigator({
    viewProfileContact : {
      screen : ProfileContact
    }
  });

const ContentMessageStack = StackNavigator({
    ViewContentMessage : {
        screen : ContentMessage,
    },
});

const images = [
    { props :{
        source : require('../../icons/egypte.jpeg') }
    },
    { props :{
        source : require('../../icons/greece.jpeg') }
    },
    { props :{
        source : require('../../icons/beach.jpeg') }
    },
    { props :{
        source : require('../../icons/iceland.jpeg') }
    },
    { props :{
        source : require('../../icons/desert.jpeg') }
    },
];

// create a component
class Profile extends Component {

    constructor(props){
        super(props);
        this.state = {
            ImagesCollection : [],
            UrlImageBackground : '',
            urlImageProfil : '',
            isModalOpened: false, 
            currentImageIndex: 0,
            nbFollowers : 10,
            nbFollowing : 4,
            nbPictures : 15,
            firstNameProfile : "Yacoub",
            nameProfile : "Ethmane",
            descProfile : "Je suis un ingénieur en développement Web",
            companyProfile: "BNP_Parisbas",
            showModalEdit : false,
            image : null,
            imageProfile :null,
            firstName : "",
            name : "",
            desc : "",
            company: "",
            error :""
        }
    }
    componentWilMount(){
        this.setState({imageProfile : {uri : require('../../icons/person.jpg')} })
    }

    
    static navigationOptions =({navigation}) => {
        return {
            title: 'MY PROFILE',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                opacity : 0.7,
                /*borderWidth: 1,
                borderBottomColor: 'white',*/
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
             }
        }
      }

    openModal =(index) => {
        this.setState({isModalOpened: true, currentImageIndex: index })
    }
    visibleModal =() =>{
        this.setState({isModalOpened : false})
    }

    _onPressBtnEdit = ()=> {
        this.setState({ showModalEdit: true });
    }

    _onPressBtnFlowers = ()=> {

    }

    _onPressBtnPictures = ()=> {

    }

    _onPressBtnFlowing = ()=> {

    }

    _onPressBtnValid =() =>{
        let {image, firstName, name, desc, company, firstNameProfile, nameProfile, descProfile, companyProfile} = this.state;
         
        if(image){
            this.setState({imageProfile : image})
        }
        
        if(firstName !='' && firstName != firstNameProfile){
            this.setState({firstNameProfile : firstName.trim()})
        }

        if(name !='' && name != nameProfile){
            this.setState({nameProfile : name.trim()})
        }

        if(desc !='' && desc != descProfile){
            this.setState({descProfile : desc.trim()})
        }

        if(company !='' && company != companyProfile){
            this.setState({companyProfile : company.trim()})
        }

        // Appel Service pour Mettre à jour le profil
        this._onPressBtnClose();
    }

    _onPressBtnClose =() =>{
        this.setState({ showModalEdit: false, firstName : '', name : '', desc :'', company :'' });
    }

    handleValidateFirstName = () =>{
        let {firstName} = this.state;
        if(!(firstName !='' && firstName.length>=2 && !(/^[^\W][[a-zA-Z]$/.test(firstName))) ){
            this.setState({firstName: '', error :`Erreur format Prenom `});
        }
    } 

    handleValidateName = () =>{
        let {name} = this.state;
        if(!(name !='' && name.length>=2 && !(/^[^\W][[a-zA-Z]$/.test(name))) ){
            this.setState({name: '', error :`Erreur format Nom `});
        }
    } 

    handleValidateDesc = () =>{
        let {desc} = this.state;
        if(!(desc !='' && desc.length>=10 && !(/^[^\W][[a-zA-Z]$/.test(desc))) ){
            this.setState({desc: '', error :`Erreur format Description `});
        }
    } 

    handleValidateCompany = () =>{
        let {company} = this.state;
        if(!(company !='' && company.length>=3 && !(/^[^\W][[a-zA-Z]$/.test(company))) ){
            this.setState({company: '', error :`Erreur format company `});
        }
    } 
    
    _renderButton = (text, onPress) => (
        <TouchableOpacity onPress={onPress}>
          <View style={styles.button}>
            <Text>{text}</Text>
          </View>
        </TouchableOpacity>
      );
    
      _pickImage = async () => {
        let result = await ImagePicker.launchImageLibraryAsync({
          allowsEditing: true,
          aspect: [4, 4],
        });
    
        console.log(result);
    
        if (!result.cancelled) {
          this.setState({ image: result.uri });
        }
      };

    _renderModalContent = () => {
        /**{image &&
                        <Image source={{ uri: image }} style={{ width: 300, height: 300 }} />}  */
        let {showModalEdit, image} = this.state;
        if(showModalEdit){
            return (<View style={styles.modalContent}>
                <Card>
                    <CardSection>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    {image && 
                        <Avatar
                        size="xlarge"
                        rounded
                        source={{ uri: image }}
                        onPress={() =>{}}
                        activeOpacity={0.7}
                        />  }   
                        
                    <Button
                        title="Charger une image"
                        onPress={this._pickImage}
                        style= {{marginTop: 10}}
                        />
                    </View>
                    </CardSection>
                    <CardSection>
                        <InputForm
                            reverseContent = {needReverseContent(I18n.locale)}
                            placeholder={this.state.firstNameProfile}
                            label= {`${I18n.t('firstNameProfile')}`}
                            value={this.state.firstName}
                            onChangeText={firstName => this.setState({ firstName })}
                            onBlur= {() => this.handleValidateFirstName()}   
                        />
                    </CardSection>
                    <CardSection>
                        <InputForm
                            reverseContent = {needReverseContent(I18n.locale)}
                            placeholder={this.state.nameProfile}
                            label= {`${I18n.t('nameProfile')}`}
                            value={this.state.name}
                            onChangeText={name => this.setState({ name })}
                            onBlur= {() => this.handleValidateName()}   
                        />
                    </CardSection>
                    <CardSection>
                        <InputForm
                            reverseContent = {needReverseContent(I18n.locale)}
                            placeholder={this.state.descProfile}
                            label= {`${I18n.t('descProfile')}`}
                            value={this.state.desc}
                            onChangeText={desc => this.setState({ desc })}
                            onBlur= {() => this.handleValidateDesc()}   
                        />
                    </CardSection>
                    <CardSection>
                        <InputForm
                            reverseContent = {needReverseContent(I18n.locale)}
                            placeholder={this.state.companyProfile}
                            label= {`${I18n.t('companyProfile')}`}
                            value={this.state.company}
                            onChangeText={company => this.setState({ company })}
                            onBlur= {() => this.handleValidateCompany()}   
                        />
                    </CardSection>
                    <View style={styles.textContainer}>
                    <Text style={styles.errorTextStyle}>
                        {this.state.error}
                    </Text>
                </View>
                 </Card>
                       <View style={{flexDirection : 'row'}}>
                       <View style={{flex : 1}}></View>
                       <TouchableOpacity style={{flex : 3}} onPress={this._onPressBtnValid}>
                            <Image source={require('../../icons/work-done.png')} style={{ width: 64, height: 64 }} />
                        </TouchableOpacity>
                        <View style={{flex : 3}}></View>
                        <TouchableOpacity style={{flex : 3}} onPress={this._onPressBtnClose}>
                            <Image source={require('../../icons/close-button.png')} style={{ width: 64, height: 64 }} />
                        </TouchableOpacity>
                       </View> 
                        
                    </View>)
        }else
            return null;
    

    }
    
    render() {
        /**
         * Valeurs stockées en dur
         */
        
         let { nbFollowers, nbFollowing, nbPictures, firstNameProfile, nameProfile, descProfile, companyProfile, showModalEdit, image} = this.state;
        /**
         * acces aux styles
         */
        const {container,imageFond, profil,
            profilSup,profilSupOne, profilSupTwo, profilSupThree, 
            profilInf,profilInfText1, profilInfText2, profilInfText3,
            indicator,indicatorContent1, indicatorContent2, indicatorContent3,indicatorContentText2, indicatorContentText1,
            groupImage, groupImageSection1,groupImageSection2,groupImageSection1Item,groupImageSection2Item,
            groupImageSyle
            } = styles;  
        


        return (
            <View style={container}>
            
                <View style= {profil}>
                    <BackgroundImage urlImage = {require('../../icons/landscape.jpg')} isOverlay={true} >
                    <View style={{ alignSelf: 'flex-end'}}>
                        <TouchableOpacity onPress={this._onPressBtnEdit}>
                            <Image
                                source={require('../../icons/edit-btn.png')}
                            />
                        </TouchableOpacity>
                    </View>
                        <View style= {profilSup}>
                                <View style= {profilSupTwo}>
                                        <Avatar
                                            size="xlarge"
                                            rounded
                                            /*source={require('../../icons/person.jpg')} **/
                                            source={{uri: image }}
                                            onPress={() =>{}}
                                            activeOpacity={0.7}
                                            />
                                </View>
                        </View>

                        <View style= {profilInf}>
                            <Text style= {profilInfText1}> {firstNameProfile + " "+ nameProfile} </Text>
                            <Text style= {profilInfText2}> {descProfile}</Text>
                            <Text style= {profilInfText3}> work at @{companyProfile}</Text>
                        </View>

                        <View style= {indicator}>
                            <View style= {indicatorContent1}>     
                                <TouchableOpacity onPress={this._onPressBtnFlowers}>
                                    <Text style= {indicatorContentText1}> {nbFollowers} </Text>
                                    <Text style= {indicatorContentText2}> Followers </Text>
                                </TouchableOpacity>
                            </View>
                            <View style= {indicatorContent2}>
                                <TouchableOpacity onPress={this._onPressBtnPictures}>
                                    <Text style= {indicatorContentText1}> {nbPictures} </Text>
                                    <Text style= {indicatorContentText2}> Pictures </Text>
                                </TouchableOpacity>      
                            </View>
                            <View style= {indicatorContent3}>
                                <TouchableOpacity onPress={this._onPressBtnFlowing}>
                                    <Text style= {indicatorContentText1}> {nbFollowing} </Text>
                                    <Text style= {indicatorContentText2}> Following </Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                     </BackgroundImage>
                 </View>
            
               
               <View style= {groupImage}>
                    <View style= {groupImageSection1}> 

                        <View style={groupImageSection1Item} key={0}>
                            <TouchableWithoutFeedback onPress={() => {this.openModal(0)}}>
                                <Image
                                    resizeMode="cover"
                                    style = {groupImageSyle} source={require('../../icons/egypte.jpeg')}
                                />
                            </TouchableWithoutFeedback>
                        </View>
                        <View style={groupImageSection1Item} key={1}>
                            <TouchableWithoutFeedback onPress={() => {this.openModal(1)}}>
                                <Image
                                    resizeMode="cover"
                                    style = {groupImageSyle} source={require('../../icons/greece.jpeg')}
                                />
                            </TouchableWithoutFeedback>
                        </View>            
                    </View>

                    <View style= {groupImageSection2}> 

                    <View style={groupImageSection2Item} key={2}>
                        <TouchableWithoutFeedback onPress={() => {this.openModal(2)}}>
                            <Image
                                resizeMode="cover"
                                style = {groupImageSyle} source={require('../../icons/beach.jpeg')}
                            />
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={groupImageSection2Item} key={3}>
                        <TouchableWithoutFeedback onPress={() => {this.openModal(3)}}>
                            <Image
                                resizeMode="cover"
                                style = {groupImageSyle} source={require('../../icons/iceland.jpeg')}
                            />
                        </TouchableWithoutFeedback>
                    </View>

                    <View style={groupImageSection2Item} key={4}>
                        <TouchableWithoutFeedback onPress={() => {this.openModal(4)}}>
                            <Image
                                resizeMode="cover"
                                style = {groupImageSyle} source={require('../../icons/desert.jpeg')}
                            />
                        </TouchableWithoutFeedback>
                    </View>
                        
                    </View>
               </View>

               <ModalRN visible={this.state.isModalOpened} transparent={true} onRequestClose={() => { this.visibleModal()}}>
                    <ImageViewer imageUrls={images} index={this.state.currentImageIndex}/>
               </ModalRN>
               <Modal isVisible={showModalEdit}
                        animationIn={'slideInLeft'}
                        animationOut={'slideOutRight'}
                        animationInTiming={500}
                        animationOutTiming={500}
                        backdropTransitionInTiming={500}
                        backdropTransitionOutTiming={500}
                >
                    {this._renderModalContent()}
                </Modal>
            </View>
        );
    }   
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,  
    },
    imageFond : {
        position : 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width : '100%',
        height : '100%',

    },
    profil : {
        flex: 7, 
        width : '100%',
        height : '100%',
        //flexDirection : 'column'
    },
    profilSup:{
        flex: 13,
        flexDirection : 'row'
    },
    profilSupOne: {
        flex : 1,
        width : '100%',
        alignSelf : 'center',
        alignItems : 'flex-end'
    },
    profilSupTwo: {
        flex : 1,
        width : '100%',   
        alignSelf : 'center',
        alignItems :'center'
    },
    profilSupThree: {
        flex : 1,
        width : '100%',
        alignSelf : 'center'
    },
    profilInf: {
        flex : 7,
        alignSelf : 'center'
        
    },

    profilInfText1 : {
        fontWeight : 'bold',
        fontSize : 18,
        color : 'white',
        alignSelf : 'center'
    },
    profilInfText2:{
        marginTop : 2,   
        color : 'white',
        alignSelf : 'center'
        
   },
    profilInfText3 :{
        fontWeight : 'bold',
        color : 'white',
        alignSelf : 'center'       
     },
    indicator : {
        flex: 2,
        flexDirection : 'row',
        alignSelf :'center',
        color : 'white',
        paddingBottom: 15
    },
    indicatorContent1 : {
        flex: 1,
        alignSelf :'center',
        borderRightColor: 'white',
        borderRightWidth: 1

    },
    indicatorContent2 : {
        flex: 1, 
        alignSelf :'center',
        borderRightColor: 'white',
        borderRightWidth: 1
    },
    indicatorContent3 : {
        flex: 1,
        alignSelf :'center'
    },
    indicatorContentText1 :{
        alignSelf :'center',
        fontSize : 18,
        fontWeight : 'bold',
        color : 'white',

    },
    indicatorContentText2: {
        alignSelf :'center',
        color : 'white',
        opacity : 0.6   
    },

    groupImage : {
        flex: 8, 
    },
    groupImageSection1 : {
        flex : 3,
        flexDirection : 'row'
    },
    groupImageSection1Item :{
        flex : 1
    }, 
    groupImageSection2 : {
        flex : 2,
        flexDirection : 'row'
    },
    groupImageSection2Item :{
        flex : 1
    },
    groupImageSyle : {
        width : '100%',
        height : '100%',  
    },
    modalContent: {
       backgroundColor: 'white',
       
        borderRadius: 20,
        justifyContent: 'center',
        borderColor: 'rgba(0, 0, 0, 0.1)'
      },
      button: {
        backgroundColor: 'lightblue',
        padding: 8,
        margin: 10,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
        borderColor: 'rgba(0, 0, 0, 0.1)',
      },
      textContainer: {
        paddingLeft: 8,
        margin: 5,
    },
    errorTextStyle : {
        fontSize: 16,
        alignSelf: 'center',
        color : APP_COLORS.accent,
    },
});

//make this component available to the app
export default Profile;
/**
 * <Avatar
                                            size="small"
                                            rounded
                                            source={require('../../icons/edit-button.png')}
                                            onPress={() => {
                                                console.log(this.props); this.props.navigation.navigate('viewProfileContact')
                                            }}
                                            activeOpacity={0.7} 
                                            />
 */ 