import React, { Component } from 'react';
import { Image, StyleSheet } from 'react-native';
import { Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body, Right } from 'native-base';
import {APP_COLORS} from '../style/color';

class CardTimeLine extends Component {
  constructor(props) {
    super(props);
    this.state = {
      card: {
        avatar: '',
        personne: 'Yac',
        place: 'New York',
        like: 12,
        comments: 14,
      }
    }
  }
  
  render() {
    const {icone, text, image}  = styles;
    return (

          <Card>
            <CardItem>
              <Left>
                <Thumbnail source={require('./icons/landscape.jpg')} />
                <Body>
                  <Text>{this.state.card.personne}</Text>
                  <Text note>{this.state.card.place}</Text>
                </Body>
              </Left>
              <Right>
               <Text style={text}>11h ago</Text>
              </Right>
            </CardItem>
            <CardItem cardBody>
              <Image source={require('./icons/landscape.jpg')} style={image}/>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent>
                  <Image source={require('./icons/heart.png')} style={icone}/>
                  <Text style={text}>{this.state.card.like}</Text>
                </Button>
              </Left>
              <Body>
                <Button transparent>
                <Image source={require('./icons/comment.png')} style={icone}/>
                <Text style={text}>{this.state.card.comments}</Text>
                </Button>
              </Body>
              <Right>
                <Text style={text}>11h ago</Text>
              </Right>
            </CardItem>
          </Card>
      
    );
  }

}


const styles = StyleSheet.create({
  text : {
      color: APP_COLORS.secondaryText,       
  },
  icone : {
    height: 20, 
    width: 20,   
  },
  image : {
    height: 200, 
    width: null, 
    flex: 1, 
},
});

export default CardTimeLine;