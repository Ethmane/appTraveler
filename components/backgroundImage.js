//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet , Image} from 'react-native';
import ImageOverlay from "react-native-image-overlay";

// create a component
class BackgroundImage extends Component {
    constructor(props){
        super(props);
    }
    render() {
        const {urlImage, isOverlay} = this.props;
        const {container, overlay, backgroundImageStyle} = styles;

        return (
            <View style={[{ flex: 1},{overlay}]}>
                <ImageOverlay
                        source={urlImage}
                        overlayColor="#4a4f6f"
                        overlayAlpha={0.7}>
                    <View style={container}>
                        {this.props.children} 
                    </View>
                </ImageOverlay>
            </View>
        );
    }
}

//style={isOverlay ? {overlay} : {}}
// define your styles
const styles = StyleSheet.create({
    container: {
       /* flex: 1,
        width: null,
        height: null,
        resizeMode: 'cover',*/
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0, 
        justifyContent: 'center',
        alignItems: 'center',
    },
    overlay : {
        flex: 1,
        width: null,
        height: null,
        backgroundColor : '#4a4f6f',
        opacity : 0.9,
        resizeMode: 'cover',
    },
    backgroundImageStyle : {
        flex: 1,
        resizeMode: 'cover',
        width: undefined,
        height: undefined,
        backgroundColor: '#889DAD'
    }
});

/**
 *  <View style={[{ flex: 1},{overlay}]}>
                <Image style={backgroundImageStyle} source={urlImage}/>
                <View style={container}>
                    {this.props.children} 
                </View>
            </View>
 */

//make this component available to the app
export default BackgroundImage;
