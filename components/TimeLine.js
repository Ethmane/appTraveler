//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
//import {Icon} from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import { Container, Header, Content} from 'native-base';
import CardTimeLine from './CardTimeLine';
import {APP_COLORS} from '../style/color';
// create a component
class TimeLine extends Component {
    static navigationOptions =({navigation}) => {
        return {
            title: 'TIMELINE',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
        }
        }
      }  

    render() {
        return (
        <Content>
            <CardTimeLine/>
            <CardTimeLine/>
            <CardTimeLine/>
            <CardTimeLine/>
        </Content>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        
    },
});

//make this component available to the app
export default TimeLine;
