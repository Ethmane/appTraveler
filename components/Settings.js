//import liraries
import React, { Component } from 'react';
import { View, StyleSheet, Image, ScrollView, TextInput, TouchableOpacity, Text as TextNative} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

import { Avatar } from 'react-native-elements'
import {Container,Content, Form, Item, Input, Label, H1, H2, H3, Text, Right, List, ListItem, Left, Body, Container as ContentContainer }  from 'native-base';
import Icon from 'react-native-vector-icons/EvilIcons';
import Switch from 'react-native-switch-pro';
import {APP_COLORS} from '../style/color';


// create a component 
class Settings extends Component {
    constructor(props){
        super(props);
        this.state = {
            profil: {
                userName : ' ',
                userSurName :' ',   
                userNumberPhone : ' ',
                SwitchNotif : false,
                SwitchTag : false,
                SwitchComment : false,
                urlFacebook: '',
                urlTwitter : ''
            }
        };
    }

    static navigationOptions =({navigation}) => {
        return {
            title: 'SETTINGS',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
             }
        }
      }  

    handleChangeTextUserName = (text) => {
        this.setState({profil: {userName : text}});
        console.log('userName : ', this.state.profil.userName);
    }

    handleChangeTextUserSurName = (text) => {
        this.setState({profil: {userSurName : text}});
        console.log('userSurName : ', this.state.profil.userSurName);
    }

    handleChangeUserNumberPhone = (num) => {
        this.setState({profil: {userNumberPhone : num}});
        console.log('userNumberPhone : ', this.state.profil.userNumberPhone);
    }

    onPressButtonLogOut = () => {
        console.log('logout');
    }


    render() {   
        return (
            <View style ={styles.container}>

                    <View style= {styles.imageContainer}>
                        <Image  style ={styles.imageFond} source = {require('./icons/landscape.jpg')} >
                                <Avatar
                                large
                                rounded
                                source={require('./icons/person.jpg')}
                                activeOpacity={0.7}
                                />
                       
                                <Image 
                                style= {styles.iconCamera} 
                                source= {require('./icons/camera.png')}
                                onPress={() => console.log("Works!")}
                                />
                        </Image>  
                    </View>

                    <Content style={{flex : 10}}>
                        <KeyboardAwareScrollView> 

                            <View >
                                <View style={[{flex : 1}, styles.titleView]}>
                                    <H3 style={styles.titleText}> DONNEES PERSONNELLES</H3>
                                </View>
                                
                                <Item floatingLabel style={styles.itemContainer}>
                                    <Label >  <Text style={styles.itemText}>Nom de famille</Text></Label>
                                    <Input  onChangeText={ (text) => this.handleChangeTextUserName(text)} />
                                </Item>
                                
                                <Item floatingLabel style={styles.itemContainer}>
                                    <Label >  <Text style={styles.itemText}>Prenom</Text></Label>
                                    <Input  onChangeText={(text) => this.handleChangeTextUserSurName(text)}/>
                                </Item>
                                
                                <Item floatingLabel style={styles.itemContainer} >
                                    <Label >  <Text style={styles.itemText}>Numero de Téléphone</Text></Label>  
                                    <Input keyboardType='numeric'    onChangeText={(num) => this.handleChangeUserNumberPhone(num)}/>
                                </Item>
                            </View>

                            <View >
                                <View style={[{flex : 1}, styles.titleView]}>
                                    <H3 style={styles.titleText}>RESEAUX SOCIAUX</H3>
                                </View>

                                <Item floatingLabel style={styles.itemContainer}>
                                    <Label > <Text style={styles.itemText}>Facebook</Text> </Label>
                                    <Input  onChangeText={ (text) => this.handleChangeTextUserName(text)} />
                            
                                </Item>

                                <Item floatingLabel style={styles.itemContainer}>
                                    <Label > <Text style={styles.itemText}>Twitter</Text></Label>
                                    <Input  onChangeText={(text) => this.handleChangeTextUserSurName(text)}/>
                                </Item>
                            </View>

                        </KeyboardAwareScrollView>   

                        <View >

                            <View style={[{flex : 1}, styles.titleView]}>
                                <H3 style={styles.titleText}>NOTIFICATIONS</H3>
                            </View>

                            <Item  style={[styles.switchContainer, styles.itemContainer]}>
                                <Text >Notifcations</Text>
                                <Switch 
                                width={50}
                                height={25}
                                backgroundActive={'#4b4e71'}
                                defaultValue={true}
                                onSyncPress = { (value)=>  this.setState({profil: {SwitchNotif : value}})} 
                                value = {this.state.profil.SwitchNotif} 
                                />

                            </Item>
                            <Item  style={[styles.switchContainer, styles.itemContainer]}>
                                <Text >Acceptez-vous d'etre identifier</Text>
                                <Switch 
                                width={50}
                                height={25}
                                backgroundActive={'#4b4e71'}
                                defaultValue={true}
                                onSyncPress = { (value)=>  this.setState({profil : {SwitchTag : value }})} 
                                value = {this.state.profil.SwitchTag} 
                                />

                            </Item>
                            <Item   style={[styles.switchContainer, styles.itemContainer]}>
                                <Text>Recevoir un commentaire</Text>
                                <Switch 
                                width={50}
                                height={25}
                                backgroundActive={'#4b4e71'}
                                defaultValue={true}
                                onSyncPress = {  (value)=>  this.setState({profil : { SwitchComment : value }})} 
                                value = {this.state.profil.SwitchComment} 
                                />
                            </Item>
                          
                        </View> 

                        <View >
                            <View>
                                <TouchableOpacity onPress={this.onPressButtonLogOut} style={styles.buttonLogOut}>
                                        <TextNative style={styles.textButton}>LOG OUT </TextNative>
                                </TouchableOpacity>
                            </View>
                        </View>
                        
                               
                    </Content>
          
            </View>
        );
    }
}

// define  styles
const styles = StyleSheet.create({
    container : {
        flex: 1,       
    },
    header : {
        flex: 1,
    },
    imageContainer: {
        flex: 0.7, 
        backgroundColor: 'transparent',
    },
    imageFond : {
        flex: 1,
        position : 'absolute',
        justifyContent: 'center',
        alignItems: 'center',
        width : '100%',
        height : 250,
    },
    iconCamera: {
        position : 'absolute',
        justifyContent : 'center',
        alignItems : 'center',
        opacity: 0.7,
    },
    containerText :{
        flex :1, 
    },   
    titleView :{
        backgroundColor: '#f1f1f1',
        paddingTop: '5%',
        paddingLeft: '5%',
        paddingBottom: '1%'
    },
    titleText :{
        fontSize: 20,
        color: 'black',
    },
    buttonLogOut: {
        justifyContent : 'center',
        alignItems: 'center',
        paddingVertical: 20,
        backgroundColor: '#f1f1f1',
    },
    textButton: {
        fontSize: 20,
        color: 'black',
    },
    switchContainer: {
        backgroundColor: 'transparent',
        paddingVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingRight: 5,
    },
    itemContainer: {
        marginLeft: '5%',
        marginRight: '5%',
    },
  
});

//make this component available to the app
export default Settings;
/*

 */