//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, ListView } from 'react-native';
import Icon from 'react-native-vector-icons/EvilIcons';
import FriendItem from './FriendItem';
import {APP_COLORS} from '../style/color';
// create a component
class Friends extends Component {

    static navigationOptions =({navigation}) => {
        return {
            title: 'FRIENDS',
            headerLeft: (<Icon name="chevron-left" size={35} style={{color: "#FFF",}} onPress={ () => navigation.navigate('DrawerOpen') } />),
            headerTintColor: 'lightgrey',
            headerStyle: { 
                backgroundColor: '#4a4f6f',
                borderWidth: 1,
                borderBottomColor: 'white',
                
             },
             headerTitleStyle: {
                alignSelf: 'center',
                paddingRight: 55,
                color : 'white',
               
            }
        }
    }

    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
          dataSource: ds.cloneWithRows([{"name": "Yacoub", "surname": "Ethmane"}, {"name": "Klaudia", "surname": "Ethmane"}]),
        };
      }
    displayFriendItem = (rowData) => {
        return <FriendItem data={rowData} {...this.props}/>
      
    }
    render() {
        return (
            <ListView    
                dataSource={this.state.dataSource}
                renderRow={(rowData) => this.displayFriendItem(rowData)}
            />
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});

//make this component available to the app
export default Friends;
