//import liraries
import BackgroundImage from './backgroundImage';
import React, { Component } from 'react';
import Modal from 'react-native-modal';
import { View, Text, StyleSheet, TouchableWithoutFeedback, TouchableHighlight} from 'react-native';
import { Container, Header, Content, SwipeRow, Icon, Button, List, ListItem, Left, Body, Right, Thumbnail} from 'native-base';
import { StackNavigator} from 'react-navigation';
import {APP_COLORS} from '../style/color';
import Traveler from './screens/Profile/Traveler';
import ContentMessage  from './ContentMessage';
const _require = require;


/*let getRequire = (src) =>{

    return require(src)
}*/

const ContentMessageStack = StackNavigator({
    ViewContentMessage : {
        screen : ContentMessage,
    },
  
  });

// create a component
class ItemMessage extends Component {

    constructor(props){
        super(props);
        this.state = {
            isMovingRight : false,
            modalVisible: false,
        }
    }

    componentWillMount() {
        //var textsrc = './icons/person.jpg' ;
        let source = require( `${'./icons/person.jpg'}`);
         
    }

   

    handleLongPress = () => {
        console.log("Youpi");
        this.setModalVisible(true);
    }

    setModalVisible = (visible) => {
        this.setState({modalVisible: visible});
    }

    handleDeleteMessage = () => {
        const {callbackDelete, message} = this.props;

        this.setModalVisible(!this.state.modalVisible);
        callbackDelete(message.id);
    }

    render() {
        const {textFullName, textPreview , container, modal, headerModal,bodyModal,btnAnnuler,
             btnSupprimer, textbtn, btnModal,
             textheaderModal} = styles;

        const {name, surname, iconName }= this.props.message;
        
        //const {source} = this;
       
        //const src = `${'./icons/person.jpg'}`;
        // const src = './icons/person.jpg';
       // let source = eval(`require(${src})`);
        let source = require('./icons/person.jpg');
       // const source = require(`${src}`);
    
        //let source = eval("5 + 5");
       
        console.log('source : ' , source);
        return (
            <View style= {container}>
                    <Modal
                        
                        animationIn={'zoomInDown'}
                        animationOut={'zoomOutUp'}
                        animationInTiming={1000}
                        animationOutTiming={2000}
                        backdropTransiitionInTiming={1000}
                        backdropTransiitionOutTiming={1000}
                        isVisible={this.state.modalVisible}
                        onRequestClose={() => {alert("Modal has been closed.")}}>
                        <View style={modal}>
                            <View>
                               <View style={headerModal}>
                                    <Text style={textheaderModal}>Voulez-vous supprimer ces messages ?</Text>
                               </View>

                               <View style={bodyModal}>

                               <View style={btnModal}>
                                <Button style={btnSupprimer} onPress={() => this.handleDeleteMessage()}>
                                        <Text style={textbtn}>SUPPRIMER</Text>
                                    </Button>
                               </View>

                               <View style={btnModal}>
                                <Button style={btnAnnuler} onPress={() => {
                                    this.setModalVisible(!this.state.modalVisible)
                                    }}>
                                    <Text style={textbtn}>ANNULER</Text>
                                </Button>
                               </View>
                               </View>
                                
                            </View>
                        </View>
                </Modal>



            <TouchableWithoutFeedback  onLongPress={ () => this.handleLongPress()} onPress={ () => { console.log(this.props); this.props.navigation.navigate('ViewContentMessage') }} >
               <ListItem avatar>
              <Left>
                <Thumbnail  source={source} />
              </Left>
              <Body>
                <Text style= {textFullName} >{surname} {name}</Text>
                <Text style= {textPreview}note>Doing what you like will always keep you happy . .</Text>
              </Body>
              <Right>
                <Text note> {Math.floor(Math.random()* 12)}:{Math.floor(Math.random()* 60)} pm</Text>  
              </Right>
            </ListItem>
            </TouchableWithoutFeedback>
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderRadius : 1,
        backgroundColor : "#FFFFFF"  
    },
    textFullName: {
        fontSize : 16,
        fontWeight : 'bold',
    }, 
    textPreview : {
        fontSize : 10
    },
    modal : {  
        height : 200,  
        backgroundColor : '#FFFFFF',
        justifyContent : 'center',
        alignItems : 'center',
        borderRadius : 25,
        borderColor : "#8285d6",
        padding : 5
    },
    headerModal : {
        flex: 1, 
        width : '100%', 
        height : '100%',
        justifyContent : 'center', 
       
        
    },
    textheaderModal : {
        color : 'grey',
        fontWeight : 'bold',
        fontStyle: 'italic',
        fontSize : 16,
    },
    bodyModal :{
        flex: 1, 
        flexDirection : 'row',
        
        height : '100%',
        justifyContent : 'space-between',
       
        
    },
    textbtn : {
        color : '#FFFFFF',
        fontSize : 16,
        fontWeight : 'bold',
    },
     
     btnAnnuler : {
        backgroundColor : '#8285d6',
        height: 50,
        width : 140,
        alignSelf : 'center',
        justifyContent : 'center',
        marginLeft : 20,
    },
     btnSupprimer: {
        backgroundColor :'red',
        height: 50,
        width : 140,
        alignSelf : 'center',
        justifyContent : 'center',
        marginRight : 20,
     },

     btnModal : {
        justifyContent : 'center', 
        alignSelf : 'center'
    }
});

//make this component available to the app
export default ItemMessage;
